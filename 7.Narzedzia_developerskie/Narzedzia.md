# Struktury katalogow
- sass/ - w którym tworzymy i edytujemy pliki *.scss,
- css/ - w nim znajdują się wyłącznie pliki wygenerowane przez Sassa,
- vendor/ - jest przeznaczony na pliki bibliotek (np. Bootstrap), których nie edytujemy,
- images/ - w którym znajdują się obrazki, z których korzysta nasz projekt,
- js/ - w nim już niedługo będziemy tworzyć pliki z kodem JavaScript.

# Sublime Text
https://www.sublimetext.com
https://packagecontrol.io/installation - Instalacja Wtyczek
https://emmet.io/ - Emmet
https://www.smashingmagazine.com/2016/06/shortcuts-and-tips-for-improving-your-productivity-with-sublime-text/ - Sublime Porady
https://www.youtube.com/watch?v=SVkR1ZkNusI&list=PLpcSpRrAaOaqQMDlCzE_Y6IUUzaSfYocK - Sublime Text

## Inne edytory
- Atom - https://atom.io/,
- Brackets - http://brackets.io/,
- Web Storm lub PHP Storm - https://www.jetbrains.com/
- Visual Studio Code - https://code.visualstudio.com/

# Terminal
https://kodilla.com/static/bootcamp/Poradnik%20-%20Obs%C5%82uga%20linii%20komend.pdf
- * asterisk / && 
- Ścieżka - pwd
- katalog domowy - ~
- ls lista
- mkdir - nowy katalog
- Ciągłe śledzenie --watch
- cd ~ - przejdź do katalogu domowego
- touch - tworzenie pustych plików
- flaga -r - kopiowanie razem z zawartością 
- usuwanie -rm
- usuwanie pstych katalogów -rmdir
- ls -R - wyświetla listę plików i
          katalogów nie tylko dla aktualnego katalogu roboczego terminala, ale też dla
          każdego listowanego katalogu
### ls -a :
● wszystkie pliki i katalogi w danej lokalizacji, włącznie z ukrytymi, <br>
● pozycję .. , która oznacza ścieżkę do katalogu wyższego poziomu (tzn.
katalogu, w którym znajduje się katalog, którego zawartość listujemy), <br>
● pozycję . , która oznacza ścieżkę do obecnie przeglądanego katalogu (czyli
"tutaj").

### cp  (copy)
cp _header.scss _grid.scss.

### mv (move)
mv folder


### Czytanie
cat
less
Wyjscie - q

### Edytor nano
nano plik

### Edytor VIM
esc + :wq lub : q!

###Add sublime to editor
ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" /usr/local/bin/subl

# NPM
- https://www.npmjs.com
- https://www.sitepoint.com/beginners-guide-node-package-manager
- https://medium.com/@jontorrado/working-with-npm-and-yarn-289af6069168 // TODO

NPM pozwala na zainstalowanie olbrzymiej liczby pakietów. Jako przykłady można wymienić narzędzia służące do:
- sprawdzania poprawność kodu,
- łączenia oraz minifikacji plików, która oznacza m.in. usunięcie spacji, tabów i nowych linii, dzięki czemu zmniejszy się rozmiar plików pobieranych przez każdego odwiedzającego naszą stronę,
- wstawiania fragmentów kodu, np. tej samej stopki na wszystkich podstronach,
- dodawania w css właściwości z prefiksami przeglądarek, jeśli są wymagane, np. display: -webkit-flex,
- kasowania niepotrzebnych plików, np. “osieroconego” pliku .css wygenerowanego kiedyś z pliku .scss, który został już usunięty,
- tworzenia paczki .zip całego projektu,
- automatycznego odświeżania strony po zapisaniu zmian w plikach, co jest szczególnie przydatne przy pracy na dwóch ekranach.

##PackageJson
  "scripts": {
    "test": "npm run test:html && npm run test:js",
    "test:html": "nu-html-checker *.html",
    "test:js": "jshint *.js",
    "build": "npm run build:sass && npm run build:autoprefixer && npm run test",
    "build:clean": "rimraf css/*css",
    "build:sass": "sass --sourcemap=none --update sass:css",
    "build:autoprefixer": "autoprefixer-cli css/style.css",
    "watch": "parallelshell \"npm run watch:sass\" \"npm run watch:autoprefixer\"  \"browser-sync start --server --files \" ",
    "watch:sass": "sass --watch sass:css",
    "watch:autoprefixer": "onchange -d 100 css/style.css -- autoprefixer-cli -o css/style.prefixed.css css/style.css",
    "watch:browsersync:": "browser-sync start --server --files"
},
  "devDependencies": {
      "autoprefixer-cli": "^1.0.0",
      "browser-sync": "^2.18.13",
      "jshint": "^2.9.5",
      "nu-html-checker": "^0.1.0",
      "onchange": "^3.2.1",
      "parallelshell": "^3.0.2",
      "rimraf": "^2.6.2"
}
    
# Task Runner
![Taskrunner](../image/taskrunner.png)

- Grunt
- Gulp
- Webpack

# GIT
- https://git-scm.com
- http://rogerdudler.github.io/git-guide/

config --global user.name "Jan Kowalski".
- git config --global user.email "jan.kowalski@example.com"
- git config --global -l

- git log --graph --decorate --pretty=oneline --abbrev-commit

- git init
- git status
- git add .
- git commit -m "Add index.html"
- git tree - historia 

# Autoprefixer
- https://www.npmjs.com/package/autoprefixer


# IDEA

## File Watcher 
- https://www.jetbrains.com/help/phpstorm/transpiling-sass-less-and-scss-to-css.html


# Metodologia BEM
- https://en.bem.info


# Emmet