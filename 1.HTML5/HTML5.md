# 1.HTML 5
- http://html5doctor.com

## Sekcja HEAD*** <br>
meta - title - link - script

## Tagi HTML 5
https://www.w3schools.com/html/html5_new_elements.asp

### Zagnieżdząnie tagów
●	child (dziecko) - p jest dzieckiem body,
●	parent (rodzic) - body jest rodzicem p,
●	sibling (rodzeństwo) - h1 jest rodzeństwem img i p.


## Box Model
border: 10px 5px 10px 5px;<br>
border-top: 10px; border-right: 5px; border-bottom: 10px; border-left: 5px;
![BoxModel](../image/boxmodel.png)


##HTML 5 Mock

## Header
### Wyśrodkowany nagłówek
```css
header { padding: 30px; background-image: url(https://static.pexels.com/photos/...); } 
header h1 { font-weight: bold; text-transform: uppercase; font-size: 48px; } 
header p, header h1 { text-align: center; color: white; }
```


### Kaskadowośc stylów
Od najważniejszych:
1.	styl lokalny (inline),
2.	rozciąganie stylu (span),
3.	wydzielone bloki (div),
4.	wewnętrzny arkusz stylów,
5.	import stylów do wewnętrznego arkusza,
6.	zewnętrzny arkusz stylów,
7.	import stylów do zewnętrznego arkusza,
8.	atrybuty prezentacyjne HTML - np. color="...", width="..." i inne).

## Google Fonts
@import url(https://fonts.googleapis.com/css?family=Montserrat:400,700); <br>
@import url(https://fonts.googleapis.com/css?family=Rubik:400,500);


## Nazewnictwo
http://getbem.com/naming/ <br>
https://smacss.com <br>
http://suitcss.github.io

## Elementy blokowe i liniowe
Wartości display <br>
●	block - element blokowy. Element tego rodzaju zaczyna się zawsze od nowej linijki i zajmuje całą dostępną szerokość. Elementom które domyślnie są blokowe nie musisz nadawać właściwości width: 100%; ponieważ domyślnie zajmują 100% dostępnej przestrzeni! :) Przykładowe elementy domyślnie blokowe to ```html <div>, <h1> - <h6>, <p>,<form>, <footer>, <section>.``` <br>
●	inline - element liniowy. Nie zaczyna się od nowej linijki i zajmuje tylko tyle miejsca, ile jest niezbędne. Przykładowe elementy domyślnie wyświetlane jako inline to ```html <span>, <a>,<img>.``` <br>
●	inline-block - element liniowo-blokowy. Podobnie jak element liniowy wyświetlany jest w jednej linii. Różnica polega na tym, że takiemu elementowi można nadać określoną wysokość i szerokość (width oraz height). Ma on często zastosowanie w pasku nawigacji strony, gdzie poszczególne linki do podstron wyświetlane są w jednej linii i mają określone wymiary. <br>
●	none - element taki nie zostanie wyświetlony, jego miejsce może być zajęte przez inne elementy. Może to mieć zastosowanie, jeśli chcemy wyświetlić jakiś element np. dopiero w momencie, gdy użytkownik najedzie kursorem na ikonę.


## Float i align
Float: https://codepen.io/search/pens?q=float&limit=all&type=type-pens <br>
Align: https://codepen.io/search/pens?q=align&order=popularity&depth=everything&show_forks=false <br>
Dająć elementowi właściwośc float staje się on elementem blokowym możemy nadać width oraz height.

## Właściwość clear
Problemem floata jest to, że elementy z taką właściwością czasami "uciekają" poza swój kontener. Mamy ```html<div>``` z jakimś tłem, a w nim tekst i zdjęcie, które ma być po prawej stronie tego tekstu, więc ```html<img>``` będzie mieć float: right;. Jeśli zdjęcie jest wyższe niż tekst po lewej, "wyjedzie" ono na dole poza granice diva. 
```css
img {
    float: left;
}

p.clear {
    clear: both;
}
```
## Pozycje

### Static
Domyślną wartością dla tej właściwości jest static, co oznacza, że element będzie umieszczony w normalnym przepływie układu strony, a deklaracje offsetu będą ignorowane.


### Relative
Nadanie elementowi pozycji relative oraz wskazanie tzw. offsetu przesunie go względem pierwotnego położenia, jak w poniższym przykładzie. Taka reguła przesunie wszystkie elementy z klasą example o 20px w lewo względem ich pierwotnej pozycji.
```css
.example { position: relative; left: 20px; }
```
### Absolute
Położenie ELEMENTU nie będzie mieć wpływu na pozostałą treść ), a jego pozycja będzie określona względem najbliższego rodzica tego elementu o pozycji innej niż static. 
Element div z klasą parent ma pozycję relative

### Fixed
W tym wypadku element będzie pozycjonowany względem okna przeglądarki. 

Aby stworzyć belkę przyklejoną do góry przeglądarki, jak w przykładzie poniżej, nadajemy elementowi position: fixed; oraz odległość od górnej krawędzi okna przeglądarki top: 0;.
