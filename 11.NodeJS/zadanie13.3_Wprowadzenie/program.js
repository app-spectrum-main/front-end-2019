//Przyjmowanie danych
process.stdin.setEncoding('utf-8');
process.env.username = 'Patryk';
console.log(process.env.username);
console.log(process.version);
// process.stdin.on('readable', function() {
//     // tutaj treść tego co ma się wykonać w momencie odczytania wejścia.
// });

//Zwaracanie danych
process.stdin.on('readable', function() {
    // metoda .read() ma za zadanie odczytać co użytkownik podał na wejściu
    var input = process.stdin.read();
    if (input !== null) {
        var instruction = input.toString().trim();
        switch (instruction) {
            case '/exit':
                process.stdout.write('Quit app!');
                process.exit();
                break;
            case '/node-version':
                process.stdout.write('Node version: ' + process.versions.node + '\n');
                break;
            case '/language':
                process.stdout.write('Language' + process.env.LANG + '\n');
                break;
            default:
                process.stderr.write('Wrong instruction\n');
                break;
        }
        process.stdout.write(input);
        //Wersja z If
        // if (instruction === '/exit') {
        //     process.stdout.write('Quitting app!\n');
        //     process.exit();
        // } else {
        //     process.stderr.write('Wrong instruction!\n');
        // }
    }
});


