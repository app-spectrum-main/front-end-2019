#Dokumentacja
https://nodejs.org/api/os.html

Do bezpośredniej komunikacji ze środowiskiem, Node używa trzech kanałów komunikacyjnych:
process.stdin - standard input służący do odczytu (przyjmowania informacji z zewnątrz do aplikacji)
process.stdout - standard output służący do wypisywania komunikatów z procesu
process.stderr - standard służący do informowania o błędach

#Obiekt Global

#Obiekt Process
process.versions
```js
// console.log(process.versions);
{ http_parser: '2.7.1',
  node: '6.5.0',
  v8: '5.1.281.81',
  uv: '1.9.1',
  zlib: '1.2.8',
  ares: '1.10.1-DEV',
  icu: '57.1',
  modules: '48',
  openssl: '1.0.2h'
}
```
process.env
```js
{
  LANG: 'pl_PL.utf8',
  WEBIDE_JDK: '/usr/lib/jvm/default',
  SHLVL: '3',
  XDG_SEAT: 'seat0',
  HOME: '/home/hello',
  LOGNAME: 'hello',
}
```

#Przyjmowanie danych od użytkownika
```js
process.stdin.setEncoding('utf-8');
process.stdin.on('readable', function() {
    // tutaj treść tego co ma się wykonać w momencie odczytania wejścia.
});

/*Zwracanie danych od użytkownika*/

process.stdin.on('readable', function() {
    // metoda .read() ma za zadanie odczytać co użytkownik podał na wejściu
    var input = process.stdin.read();
    if (input !== null) {
        var instruction = input.toString().trim();
        if (instruction === '/exit') {
            process.stdout.write('Quitting app!\n');
            process.exit();
        } else {
            process.stdout.write('Wrong instruction!\n');
        }
    }
});

```

#Moduły
https://auth0.com/blog/javascript-module-systems-showdown/

#CommonJS
```js
var module = require('nazwa-modułu');
```
#MODUŁY
- CommonJS
- AMD (ang. Asynchronous Module Definition)
- Native JS (ES6 modules)

var module = require('nazwa-modułu');

**Import modułów**
require('http'); // paczka, która jest dostępna po zainstalowaniu node'a
require('colors'); // paczka pobrana z internetu za pomocą npm ( o którym powiemy sobie nieco później)

**fs (file system)**
fs.Stats 








