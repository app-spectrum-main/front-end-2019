//Callback - wywołanie zwrotne funkcji
function showInfo(callback){
    console.log('Kurs Node.js');
    setTimeout(function (){
        callback();
        }, 3000);
}

showInfo(function(){
    console.log('Funkcja callback');
});

