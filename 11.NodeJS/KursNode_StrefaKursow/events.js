// var events = require('events');
// var emiter = new events.EventEmitter();
//
// emiter.on('userRegistered', showGreetings);
//
// function showGreetings() {
//     console.log('Witaj w naszym serwisie');
// }
//
// emiter.emit('userRegistered');

var events = require('events');
var emiter = new events.EventEmitter();

emiter.on('userRegistered', function(username){
    console.log(username + ': Witaj w naszym serwisie');
});

emiter.on('userRegistered', function(){
    console.log('Witaj w naszym serwisie2');
});

emiter.on('userRegistered', function(){
    console.log('Witaj w naszym serwisie3');
});


emiter.emit('userRegistered', 'Patryk');
