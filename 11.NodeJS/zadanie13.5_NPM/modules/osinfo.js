var os = require('os');
var colors = require('colors');
var timeConverter = require('./timeConvert');

function getOSinfo() {
    var type = os.type();
    if(type === 'Darwin') {
        type = 'OSX';
    } else if(type === 'Windows_NT') {
        type = 'Windows';
    }
    var release = os.release();
    var cpu = os.cpus()[0].model;
    var uptime = os.uptime();
    var userInfo = os.userInfo();
    console.log('System:'.grey, type);
    console.log('Release:'.red, release);
    console.log('CPU model:'.blue, cpu);
    console.log('Uptime: ~'.green, (uptime / 60).toFixed(0), 'min');
    timeConverter.print(os.uptime());
    console.log('User name:', userInfo.username);
    console.log(colors.cyan('Home dir:'), userInfo.homedir);
}

exports.print = getOSinfo;
