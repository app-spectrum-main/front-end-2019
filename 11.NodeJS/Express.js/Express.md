#Express JS

Frameworki: Loopback, Sailsa, Kraken<br>
#Instalacja
npm init - inicjalizacja projektu<br>
npm install express --save<br>
npm install body-parser --save<br>
Pakiet pozwalający na obsługę różnych formatów danych w middleware takich jak JSON, text czy tzw. surowe dane (ang. raw data). <br>

Protokół HTTP oraz URI (ang. Uniform Resource Identifier)
#MVC
<p>
•	Model jest reprezentacją logiki aplikacji / problemu z jakim się zmagamy / domeną.<br>
•	Widok opisuje w jaki sposób coś wyświetlić. W React są to komponenty (szczególnie te prezentacyjne).<br>
•	Kontroler przyjmuje dane od użytkownika aplikacji i reaguje na jego działania w obrębie widoku. Aktualizuje widok i model aplikacji.<br>
</p>

#Metody HTTP
•	GET - najprostsza z metod HTTP - służy do pobierania zasobów z serwera. Pobranymi zasobami mogą być np. pliki HTML, CSS, JavaScript czy obiekty JSON / XML.<br>
•	POST - ta metoda jest używana do wysyłania danych do serwera. Stosuje się ją np. przy formularzach lub przy wstawianiu zdjęć i wysyłaniu ich jako załącznik. Zwykle dane te wysyłane są jako para klucz-wartość.<br>
•	PUT - działa podobnie jak POST, czyli również służy do wysyłania danych. Różnicą jest ograniczenie do wysłania tylko jednej porcji danych - np. jednego pola. Metoda ta najczęściej używana jest do aktualizacji istniejących danych<br>
•	DELETE - metoda, która służy do usuwania danych z serwera. Chodzi oczywiście o dane, które zostały wskazane przy wysyłaniu żądania.<br>
Żądania klienta - endpointy
***req.params.id***

**Obsluga bledow**
•	400 - bad request - występuje gdy serwer nie może przetworzyć zapytania<br>
•	401 - unauthorized - występuje gdy wymagane jest uwierzytelnienie, a nie zostało dostarczone<br>
•	403 - forbidden - żądanie jest poprawne, jednak serwer odmawia odpowiedzi, może to wystąpić w przypadku gdy np. użytkownik jest zalogowany ale nie ma uprawnień do wykonania żądania<br>
•	404 - not found - zasoby nie zostały znalezione<br>
•	500 - internal server error - występuje gdy występują nieznane warunki i nie ma żadnej konkretnej wiadomości<br>

JSON.stringify
express.static

**Middleware**
<p>Middleware, czyli pośrednik między żądaniem a odpowiedzią jest funkcją. Ma ona dostęp do obiektu żądania (req) oraz obiektu odpowiedzi (res). Posiada ona jeszcze trzeci parametr - next, który jest funkcją wykonującą kolejny krok w cyklu żądanie-odpowiedź.
</p>
<img src="https://kodilla.com/static/img/js-images/js-5_12.png">

**Szablony HTML PUG**
Funkcje
Atrybuty
Przekazywanie zmiennych
Warunki logiczne
Komponenty i łaczenie plików

**Autoryzacja Google**
npm install express passport passport-google-oauth
