## Adaptive Web Design
W takim wypadku strona mobilna (smartfony, tablety) i desktopowa (komputery stacjonarne, laptopy) to dwa oddzielnie realizowane projekty. Inny szablon front-endowy buduje się dla smartfona, a inny dla laptopa.

## Takie podejście sprawia jednak, 
że trzeba napisać znacznie więcej kodu, by je stworzyć. Także wszelkie zmiany wyglądu, np. na stronie głównej, wymagają modyfikacji nie w jednym, ale w dwóch szablonach. Takie podejście jest stosowane, kiedy: <br>
●	grafik/projektant z jakiegoś powodu założył, że wersja mobilna będzie się bardzo różnić od desktopowej i łatwiej jest wtedy stworzyć oddzielny szablon, <br>
●	witryna ma ogromny ruch internetowy i przeznaczony dla mobile szablon jest zoptymalizowany pod kątem wydajności i szybkości ładowania versus obciążenie serwerów.

## Media queries

### Breakpointy
```css
@media (max-width: 600px) {
 /* Niżej dodajemy reguły CSS, które zadziałają tylko  dla ekranów o maksymalnej szerokości 600px */     
.class { color: #000; } }
```

### Rodzaje urządzeń
- all
- screen
- tv
- print
- braille 
- 3d-glasses


### Operatory logiczne
```css
@media screen and (min-width: 800px) and (max-width: 1024px) {...}
```


### Warunki
1.	Min-width i max-width - określające szerokość urządzenia (a dokładnie viewportu, o czym przeczytasz poniżej).
2.	Orientation - orientacja pionowa lub pozioma urządzenia. Dostępne wartości to portrait (wysokość ekranu jest większa niż szerokość) oraz landscape (na odwrót). Ma to szczególnie zastosowanie w dostosowywaniu strony do wyświetlacza tabletu lub telefonu, które mogą być trzymane przez użytkownika zarówno pionowo jak i poziomo.
3.	Resolution - przy jej pomocy możemy określić reguły CSS, które będą miały zastosowanie tylko, jeśli "gęstość upakowania pikseli" w urządzeniu będzie miała określoną wartość (podanej w dpi, czyli plamce na cal lub dpcm - plamce na centymetr).

![GRID](../image/rwd.png)


### Flexbox
http://the-echoplex.net/flexyboxes
https://codepen.io/kodilla/pen/5f2a660935d96ebaf17c090017257bf3

```css
.flex-container { display: -webkit-flex; display: flex; }
.inline-flex-container { display: -webkit-inline-flex; display: inline-flex; }

```

### Flex-direction
Ustala kierunek i zwrot głównej osi (main-axis). Domyślną wartością jest row, czyli rozmieszczenie elementów w poziomym rzędzie, od lewej do prawej. Za pomocą wartości column tworzymy pionową kolumnę. Możemy odwrócić kolejność elementów wewnątrz kontenera, dodając odpowiednio row-reverse oraz column-reverse.
### Flex-wrap
Domyślne elementy wewnątrz kontenera będą umieszczone w jednej linijce, jednak za pomocą tej właściwości możemy to zmienić. Gdy przypiszemy kontenerowi właściwość flex-wrap: wrap, elementy będą rozmieszczone w wielu linijkach. Podobnie jak poprzednio możemy tutaj zmienić kierunek, deklarując wrap-reverse.
### Justify-content
Pozwala na rozmieszczenie elementów wzdłuż głównej osi. Mamy tutaj następujące możliwości:

- flex-start (domyślne) - elementy będą umieszczone na początku linii,
- flex-end - na końcu linii,
- center - na środku,
- space-between - elementy będą rozciągnięte wzdłuż głównej osi; pierwszy będzie na początku linii, ostatni na końcu, a pozostałe będą rozmieszczone na środku,
- space-around - podobne do space between, jednak zachowane będą równe odległości pomiędzy poszczególnymi elementami; pierwszy i ostatni element będą oddalone od krawędzi kontenera jedynie o połowę odległości pomiędzy poszczególnymi komórkami.

### Align-items
Za jego pomocą możemy modyfikować rozmieszczenie elementów na osi poprzecznej (cross-axis). Można przyjąć, że jest to funkcjonalność podobna do justify-content, jednak w wersji dla osi poprzecznej. Podobnie jak w poprzednim przypadku mamy tutaj do dyspozycji różne możliwości.
●	flex-start - górne krawędzie elementów będą umieszczone przy górnej krawędzi kontenera,
●	flex-end - analogicznie jak w przypadku flex-start, jednak dla dolnych krawędzi: będą one dociągnięte do dolnych krawędzi kontenera,
●	center - elementy będą umieszczone centralnie na osi poprzecznej,
●	baseline - elementy będą rozmieszczone tak, by zachować ich linię bazową, tj. by litery w poszczególnych elementach były umieszczone na jednej linii,
●	stretch - zawartość będzie rozciągnięta, aby wypełnić kontener (warto przy tym zaznaczyć, że wartości min-width/max-width będą zachowane).

### Align-content
Umożliwia rozmieszczenie linii z elementami wewnątrz kontenera w sytuacji, gdy jest ich więcej niż jedna linijka (dlatego też nie będzie to mieć znaczenia wtedy, gdy nie zadeklarowaliśmy odpowiednio właściwości flex-wrap).

## Właściwości Flexboksa dla elementów wewnątrz kontenera

### Order
Elementy wewnątrz flexboksa będą domyślnie wyświetlane w takiej kolejności, w jakiej znajdują się w źródle strony. Możemy to zmodyfikować, dodając właściwość order do interesującego nas elementu. Przyjmuje on wartość liczby całkowitej.
### Flex-grow
Definiując flex-grow możemy proporcjonalnie powiększyć konkretny element, jeżeli tego potrzebujemy. Przykładowo: jeżeli wszystkie elementy mają zdefiniowany flex-grow: 1, a jeden będzie określony z flex-grow: 2, będzie on zajmował dwa razy więcej wolnego miejsca (jeżeli będzie taka możliwość wewnątrz kontenera).
### Flex-shrink
Podobnie jak w poprzednim przypadku, jednak tym razem nasz element zostanie pomniejszony.
### Flex-basis
Określa domyślny rozmiar elementu. Za nim rozmieszczona jest pozostała zawartość i przydzielone jej miejsce wewnątrz kontenera. Wartość auto powoduje, że będzie to rozpatrzone na podstawie własności width oraz height. Możemy również przydzielić rozmiar elementu na podstawie zawartości nadając wartość content.
### Flex - właściwość skrótowa
Podobnie jak w wypadku definiowania marginesów, możemy zdefiniować jednocześnie flex-grow, flex-shrink oraz flex-basis. Domyślną wartością jest flex: 0 1 auto;.
### Align-self
Ta właściwość to nic innego jak nadpisanie położenia pojedynczego elementu, które było zdefiniowane przy użyciu align-items. Dlatego też może przyjmować identyczne wartości.
