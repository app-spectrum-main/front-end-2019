# FlexBox
Szybki kurs Flexbox
- http://kodu.je/flexbox/

# Propertisy

## Parent
display:flex;

### Child
flex-direction: row (domyślna)/ row-reverse / column / column-reverse
flex-shrink - domyślnie odpowiada za kurczenie się elementów w mobile

## Osie
Zależne od flex-direction: row lub column
main-axis
cross-axis

## Reverse
flex-direction: column-reverse / row-reverse

## Osi przeciwne
jak mają zachowywać się dzieci znacznika o właściwości display:flex;
flex-wrap: no-wrap (domyslnie) / wrap / 

# Układanie elemntów
justify-content - main-axis   Bazuje na domyślnej przestrzeni ! 
flex-start (domyślny) / flex-end / center / space-around / sapce-evenly / space-between

/ align-content - Domyślnie stretch
/ align-items flex-start, flex-end, center, stretch oraz baseline

# Flex shrink


# Flex grow
flex-grow: 0; Elementy ignorują dodatkową przestrzeń

# Flex-basis

# Shorthands

## Parent
flex-flow: <'flex-direction'> || <'flex-wrap'>;
flex: auto / none / initial


## Childy
flex 1 0;   flex-grow: 1;  flex-shrink: 0;
flex: 0 100px;     flex-grow: 0; flex-basis: 100px;

flex: <'flex-grow'> <'flex-shrink'> <'flex-basis'>;
flex: 0 0 50px;
