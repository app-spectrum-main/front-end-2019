# Protokół / Adres IP
- 212.191.236.110 - Protokół internetowy to zbiór ścisłych reguł w celu nawiązania łączności. <br>
- Wysyłamy zapytanie do ISP (ang. Internet Service Provider) / Nasz dostawca internetu <br>
- www.google.pl zostaje wysłane przez dostawcę do tzw DNS'a (Domain Name System) <br>
- DNS - zadanie DNS jest przetłumaczenie adresu czytelnego dla użytkownika na adres czytelny w sieci. <br>
- W tym przypadku zamienia www.google.com na 212.191.236.110.

# Protokół HTTP
Https jest szyfrowaną wersją protokołu http

## Nagłowek http
"header name : header value"
- Request URL - wskazuje na adres, jaki został użyty w celu nawiązania połączenia.
- Request Method - informuje na temat użytej metody HTTP (będzie o nich jeszcze mowa w następnym podrozdziale). GET to metoda, która służy do pobierania zasobów.
- Status Code - służy do informowania o tym, czy zapytanie się powiodło czy też nie. Więcej o kodach statusu powiemy sobie troszkę później. 200 oznacza, że z połączeniem jest wszystko OK.
- Remote Address - adres, za pomocą którego przeglądarka nawiązuje połączenie. To jest właśnie adres po tłumaczeniu przez serwer DNS (p. podrozdział zapytanie/odpowiedź).

- Content-Type - to dzięki niemu przeglądarka wie, co zrobić z danym plikiem. W naszym przykładzie wartość tego nagłówka to text/html; charset=UTF-8. Dzięki temu plik zostanie potraktowany jako tekst html o kodowaniu utf-8.
- Set-Cookie - ten nagłówek jest zwracany w odpowiedzi serwera i ustawia w naszej przeglądarce tzw. ciasteczko, czyli informacje o sesji klienta z serwerem. Może służyć np. do zapamiętania stanu koszyka na stronie. Po więcej informacji na temat ciasteczek odsyłam tutaj.
- Cookie - dzięki niemu odsyłamy do serwera informacje o stanie naszej sesji.
- User-Agent - nagłówek, który zawiera informacje na temat klienta, za pomocą którego zostało wykonane zapytanie. Na podstawie screena można odczytać, że zapytanie zostało wykonane z przeglądarki Google Chrome v.51.0 zainstalowanej na systemie Win 10 w wersji 64-bitowej.

# Metody HTTP
- GET - jest to najprostsza z metod - jak już wspomniałem - służy do pobierania zasobów z serwera. Przykładowe zasoby to: pliki html, css, js, a także obrazki w różnych formatach. Przypominam, że dokładny opis treści, którą odsyła nam serwer, jest przechowywany w nagłówku Content-Type.
- POST - ta metoda służy do wysyłania danych do serwera. Dane te mogą być zapisane w formacie pary "klucz: wartość" lub w postaci binarnej, której używa się do przesłania plików (np. załączników w postaci zdjęcia). Z metody tej korzystamy, kiedy chcemy zatwierdzić formularz lub wysłać jakieś duże pliki, takie jak zdjęcia, filmiki, pliki tekstowe.
- PUT - jest metodą, która działa bardzo podobnie do metody POST. Ograniczeniem tej metody jest wysyłanie tylko jednej porcji danych. Za jej pomocą nie można wysłać całego formularza jako zbiór danych "klucz: para". Można wysłać tylko jedną porcję. W przypadku formularza byłoby to jedno pole. W praktyce metoda PUT używana jest najczęściej do aktualizowania danych już istniejących.
- DELETE - jak się pewnie domyślacie, służy do usuwania z serwera danych, które zostały wskazane przez zapytanie.
- HEAD - ta metoda działa bardzo podobnie do metody GET z tą różnicą, że zwracane są jedynie metadane o zasobie. Troszkę jak element w HTML-u. Dane te kryją się oczywiście pod postacią nagłówków zapytania.

# Kody odpowiedzi HTTP
- 1xx - rzadko spotykane kody informacyjne.
- 2xx - kod, który oznacza, że zapytanie klienta zostało poprawnie odebrane, zrozumiane i zaakceptowane. Przykłady:
- 200 - zapytanie się powiodło, a odpowiedź na zapytanie jest zależna od metody, której użyto do wysłania zapytania;
- 201 - symbolizuje nowo utworzony zasób na serwerze. Serwer może odpowiedzieć za pomocą właśnie tego kodu np. po dodaniu komentarza.
- 3xx - kod przekierowania. Oznacza, że klient musi podjąć pewne akcje, aby dokończyć zapytanie. Po tym zapytaniu kolejne musi być HEAD albo GET w celu pobrania informacji o zasobie albo pobrania innego zasobu.
- 4xx - błąd spowodowany działaniami użytkownika. Najpopularniejszym kodem z tego zakresu jest błąd 404, który pojawia się, kiedy chcemy się odnieść do miejsca w sieci, które nie istnieje. Innymi przykładami mogą być:
- 400 - serwer nie potrafi zrozumieć zapytania;
- 401 - nieautoryzowane zapytanie. Pojawia się, gdy pominiemy odpowiednie nagłówki autoryzacji lub podane informacje są nieprawidłowe;
- 403 - serwer zrozumiał zapytanie, ale nie zgadza się na wysłanie odpowiedzi.
- 5xx - błędy serwera. Pojawiają się, kiedy serwer nie potrafi przetworzyć informacji albo coś poszło nie tak po jego stronie. Przykładowe błędy:
- 500 - wewnętrzny błąd serwera uniemożliwiający mu spełnienie zapytania;
- 501 - funkcjonalność potrzebna do spełnienia zapytania nie jest jeszcze zaimplementowana;
- 503 - serwis nie jest w stanie w tej chwili obsłużyć zapytania.

# AJAX - to akronim od Asynchronous JavaScript and XML
- obiekt XMLHttpRequest

# Synchronicznie
Kiedy mówimy o synchroniczności kodu, mamy na myśli, że nasz skrypt wykonuje się linijka po linijce. Problem pojawia się w momencie, w którym wykonanie fragmentu kodu zajmuje naszej przeglądarce dłużej niż kilka milisekund (np. podczas pobierania danych z serwera).
JavaScript jest synchronicznym językiem programowania, a mimo tego na stronach typu Facebook albo Google nie zauważamy zjawiska zawieszenia interfejsu.

# Asynchronicznie
Funkcje takie jak setTimeout (która wykonuje się po określonym czasie) albo setInterval (która wykonuje się co określoną porcję czasu) jakoś sobie z tym radzą. Działają jakby nie po kolei. Na przykład funkcja:

setTimeout(function() {
	console.log('Hello');
}, 5000);
która ma za zadanie wyświetlić w konsoli "Hello", nie zawiesza interfejsu na pięć sekund. W tym przypadku, mimo tego że JavaScript jest synchroniczny, to funkcja wykonuje się asynchronicznie. Jak to się dzieje? Odpowiedzią na to pytanie jest tzw. pętla zdarzeń (ang. event loop).

# Event Loop Pętla zdarzeń
https://www.youtube.com/watch?v=8aGhZQkoFbQ

- Heap (pol. sterta) - jest to obszar w pamięci, który jest utworzony na potrzeby działania skryptu. JavaScript używa tego obszaru do alokowania pamięci na dane. Tej pamięci nie będziemy używać do opisu pętli zdarzeń.

- Stack (pol. stos) - jest to, podobnie jak sterta, pamięć, która jest tworzona na potrzeby działania naszego programu. Na tej pamięci odkładane są każdorazowo wywołania funkcji. Na obrazku widzimy 3 elementy na stosie. To znaczy, że funkcja main() wywołała funkcję showText(), ta wywołała funkcję setTimeout().

- WebAPIs - co prawda jeszcze nie omawialiśmy pojęcia API, ale używaliśmy już go np. do wybierania elementu z drzewa DOM albo do podpinania zdarzeń. Nie są to elementy języka JavaScript, a środowiska, w którym język działa (w tym wypadku jest to przeglądarka).

- Callback queue - czyli kolejka wywołań zwrotnych. Do tego miejsca trafiają wszystkie funkcje, które mają się wykonać po wystąpieniu pewnego zdarzenia (np. na kliknięcie, na załadowanie drzewa DOM, po skończeniu 5-sekundowego oczekiwania).

- Event loop - to nasza pętla zdarzeń, która wrzuca na stos elementy stojące w kolejce wywołań zwrotnych.

# API
to akronim Application Programming Interface, co tłumaczy się jako Interfejs Programistyczny Aplikacji. Oznacza to sposób komunikacji komponentów programistycznych między sobą.

# Endpointy 
to URLe, które - odpowiednio użyte - w odpowiedzi zwracają developerom różne dane w formacie np. JSON.

# RESTful API

## REST Representational State Transfer

# Czym jest zasób?
Zasób to obiekt, który ma określony typ, powiązane z nim dane i może być w relacjach z innymi obiektami. Przykład Imgur rozjaśni, o czym mowa.

### Zacznijmy od rodzajów zasobów:

- użytkownicy,
- albumy ze zdjęciami,
- zdjęcia.
- Przykład relacji między zasobami:

- użytkownik może mieć wiele albumów,
- album może zawierać wiele zdjęć,
- dany album może być przypisany (może należeć) tylko do jednego użytkownika,
- dane zdjęcie może należeć tylko do jednego albumu.

# Kolekcje
JSON sprawdza się idealnie do przesyłania informacji na temat zasobu, ponieważ kolekcję możemy przedstawić w prosty sposób za pomocą arraya (tablicy). Do reprezentacji pojedynczego modelu idealnie nadaje się Obiekt. Istnieją też inne formaty, takie jak XML, czy YAML, jednak coraz rzadziej się je spotyka. Dominuje zdecydowanie JSON!

# Uwierzytelnianie
## Metody uwierzytelniania
API, które służą do pobierania publicznych zasobów, nie wymagają zazwyczaj uwierzytelniania. Z takich wersji korzystaliśmy w poprzednich zadaniach. Tym razem mamy do czynienia z naszymi prywatnymi zasobami. W jaki sposób oświadczyć, że my to my? Istnieje kilka przykładowych sposobów:

- Użycie protokołu HTTPS - do nagłówków takiego zapytania dodajemy parametr, np. Authorization: Basic 1Ad23DczJ8p. Jest to najprostsza metoda, która wysyła do serwera nasze dane (login i hasło) w postaci zakodowanej. Problem przy wysyłaniu danych za pomocą zwykłego protokołu HTTP polega na tym, że kodowanie to jest znane i dane mogą wpaść w niepowołane ręce. Rozwiązaniem jest protokół HTTPS! :)
- Użycie ciasteczek - problem uwierzytelniania możemy rozwiązać również poprzez ciasteczka. Serwer wysyła nam w odpowiedzi na logowanie ciasteczka, które dodajemy do każdego kolejnego zapytania (w formie nagłówka Cookie). Ciasteczko trzyma informację o trwającej sesji. Po jakimś czasie, jeżeli nie wysyłamy żadnych zapytań, po prostu wygasa.
- Token (OAuth 2.0) - z tej metody korzystamy za każdym razem, kiedy logujemy się do systemu przy pomocy trzeciej strony (Google, Facebook, Twitter, GitHub). Logujemy się na swoje konto i potwierdzamy, że aplikacja, z której chcemy skorzystać, może wykorzystywać nasze dane. Wygląda to mniej więcej tak:
- Uwierzytelnianie jako część URLa - polega na dodaniu odpowiedniego klucza jako część adresu URL. Przykładowo: https://api.cats.com/cats?key=aa786jhdnb

