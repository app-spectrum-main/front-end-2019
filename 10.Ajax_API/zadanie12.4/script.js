var url = 'http://api.icndb.com/jokes/random';
var button = document.getElementById('get-joke');
var paragraph = document.getElementById('joke');

button.addEventListener('click', function(){
    getJoke();
});

function getJoke() {
    var xhr = new XMLHttpRequest(); // Tworzymy instację klasy
    xhr.open('GET', url);  //Otwieramy połaczenie
    xhr.addEventListener('load', function() { //Nasłuchiwanie na odpowiedź serwera
        var response = JSON.parse(xhr.response); //Callback -> response / Odpowiedź w formie JSON prasowanie na JS Obiekt
        paragraph.innerHTML = response.value.joke;
    });
    xhr.send();
}

getJoke();

