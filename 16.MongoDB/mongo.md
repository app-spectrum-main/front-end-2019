#MongoDB
### Relacyjne Bazy Danych**
### Nierelacyjne bazy danych**

## Instalacja
curl -O https://fastdl.mongodb.org/osx/mongodb-osx-x86_64-3.2.10.tgz - Pobranie paczki
tar -zxvf mongodb-osx-ssl-x86_64-3.6.4.tgz - Rozpakowanie
mkdir -p mongodb && cp -R -n mongodb-osx-x86_64-3.2.10/ mongodb - Skopiowanie plikow
sudo mv mongodb-osx-ssl-x86_64-3.6.4/ /usr/local/mongodb

mongodb-osx-ssl-x86_64-3.6.4
cd /usr/local
whoami - User Name
pwd 
clear
mongod - Uruchom Mongo serwer
mongo  - Uruchom shell
Terminal
use NAZWA_BAZY
db - aktualna baza
show dbs - lista baz danych

# Tworzenie kolekcji
![Mongo](image/mongoDB.png)

db.createCollection("nazwa_kolekcji")
show collections

![Parametry](image/parametry.png)
db.{nazwa_kolekcji}.insert()

# Typy danch
- String - najpopularniejszy typ do przechowywania danych. Wartość musi być określona kodowaniem UTF-8
- Integer - ten typ przechowuje liczby całkowite. Może to być liczba całkowita 32-bitowa lub 64-bitowa, w zależności od Twojego serwera
- Boolean - ten typ przechowuje wartości true/false
- Double - przechowuje zmiennoprzecinkowe dane
- Min/Max keys - ten typ jest używany do porównania podanej wartości do najniższych i najwyższych elementów BSON
- Arrays - przechowuje tablice albo listy wartości w jednym kluczu
- Timestamp - ten typ może być pomocny w przechowywaniu danych kiedy plik był modyfikowany bądź dodany
- Object - ten typ jest używany do zagnieżdżonych dokumentów
- Null - przechowuje wartość Null
- Symbol - używany tak jak string, jednak zarezerwowany jest dla języków, które używają specyficznych znaków
- Date - ten typ przechowuje aktualny czas i w momencie utworzenia używa go. Można również zdefiniować czas samemu tworząc obiekt Date i podając dzień, miesiąc i rok
- Object ID - ten typ danych przechowuje identyfikatory dokumentów
- Binary data - przechowuje dane binarne
- Code - ten typ jest używany do przechowywania kodu JavaScript
- Regular expression - przechowuje wyrażenia regularne

# CRUD
Create - tworzenie
Read - odczytywanie
Update - aktualizowanie
Delete - usuwanie

db.{nazwa_kolekcji}.insert(document)
db.{nazwa_kolekcji}.find()

$eq - szuka wartości, która jest dokładnie taka jak w zapytaniu
$gt - szuka wartości, która jest większa od określonej w zapytaniu
$gte - szuka wartości, która jest większa lub równa określonej w zapytaniu
$lt - szuka wartości mniejszej od podanej
$lte - szuka wartości mniejszej bądź równej od podanej
$ne - szuka wszystkich wartości, które NIE są równe podanej
$in - szuka jakiejkolwiek wartości, która będzie przekazana w tablicy
$nin - szuka wartości, które NIE są wyszczególnione w tablicy

## Tworzenie
db.{nazwa_kolekcji}.insert(document)
## Odczytanie
db.{nazwa_kolekcji}.find()
# Update
db.{nazwa_kolekcji}.update({kryterium_wyboru_dokumentu}, {nowa_wartość})
# Save
{nazwa_kolekcji}.save()
# Usuwanie
db.{nazwa_kolekcji}.remove({kryteria_usuwania})

# Mongoose

# ROBOMONGO
