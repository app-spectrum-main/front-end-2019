var styles = {
            fontSize: '1.5em',
            width: '90%',
            maxWidth: '350px'
};
Search = React.createClass({
    getInitialState() {
        return {
            searchingText: ''
        };
    },
    handleChange(event) {
        this.setState({searchingText: event.target.value});
    },
    handleKeyUp(event) {
        if (event.keyCode === 13) {
            this.props.onSearch(this.state.searchingText);
        }
    },
    render() {
        return (
            <input
                type="text"
                onKeyUp={this.handleKeyUp}
                onChange={this.handleChange}
                placeholder="Tutaj wpisz wyszukiwaną frazę"
                style={styles}
                value={this.state.searchingText}
            />
        );
    }
});