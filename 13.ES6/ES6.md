#Babel

##Kompatybilnośc
http://kangax.github.io/compat-table/es6/
https://babeljs.io

#Start
npm init
npm install --save-dev babel-cli babel-preset-latest
npm install --save-dev babel-preset-<nazwa-presetu>

- stage-0 - tylko pomysł, możliwe że stanie się kiedyś propozycją
- stage-1 - propozycja, którą warto rozwijać
- stage-2 - szkic posiadający wstępną specyfikację
- stage-3 - kandydat posiadający kompletną specyfikację i pierwsze przeglądarkowe implementacje
- stage-4 - zaadoptowane rozwiązania, które wejdą do kolejnej oficjalnej wersji języka

start: babel script.js --watch --out-file script-compiled.js

#Let i const

## Function Scope
```js
var greeting = 'Hello User!';
function greetWorld(isGreeting) {
    if (isGreeting) { // kod wykona się jeśli flaga isGreeting = true
        var greeting = 'Hello World!'; // (A) zasięg: cała funkcja
        return greeting;
    }
    return greeting; // zadziała hoisting i greeting nie będzie tym czym się spodziewamy
}
greetWorld(false); // undefined
```

## Hoisting
https://developer.mozilla.org/pl/docs/Glossary/Hoisting

# Arrow function
```js
// ES6
const numbers = [1, 2, 3];
const numbersPlusOne = numbers.map( x => x + 1 );
//ES5
var numbers = [1, 2, 3];
var numbersPlusOne = numbers.map(function (x) { return x + 1 });

() => {...} // bez parametru
x => {...} // jeden parametr
(x, y) => {...} // kilka parametrów
```

#Destrukturyzacja
```js
function getCoords() {
    return {
        x: 2,
        y: 5
    };
}

const coords = getCoords();
const x = coords.x;
const y = coords.y;
```

# Zdarzenia
var req = new XMLHttpRequest();
req.open('GET', url);
req.onload = function () {
    console.log('Załadowano dane');
};
req.onerror = function () {
    console.log('Ups, coś poszło nie tak!');
};
req.send();

# Callbacki
fs.readFile('myfile.txt', { encoding: 'utf8' },
    function (error, text) {
        console.log(text);
    }
);

#Promise
```js
function download() {
    return new Promise(
        function (resolve, reject) {
            ...
            resolve(result); //spełnienie obietnicy
            ...
            reject(error); //niespełnienie obietnicy
        });
}

read()
.then( result => write() )
.then( result => timer() )
.then( result => log() )
.catch(error => …);
```

##Wymaga dodania polyfilow
npm install --save-dev babel-polyfill
<script type="text/javascript" src="<katalog-do-babel-polyfill>/dist/polyfill.js"></script>

#Tworzenie promisow
```js
const p = new Promise(
  function (resolve, reject) { ···
      if (···) {
          resolve(value); // success
      } else {
          reject(reason); // failure
      }
  }
);
```
