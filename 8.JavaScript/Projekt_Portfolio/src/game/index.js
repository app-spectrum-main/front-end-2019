import TO_FIND from './random';
import getNum from './input';

let num = getNum();
while (num !== TO_FIND) {
    num = getNum();
}
// success
