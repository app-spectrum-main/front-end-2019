/*
 *  Skrócona instrukcja warunkowa if ... else ...
 *  
 *  (wyrażenie) ? zwróć_jeżeli_wyrażenie_true : zwróć_jeżeli_wyrażenie_false 
 *  
 *  
 */

var x = 5;

/*var czyParzysta;

if (x % 2 === 0)
    czyParzysta = true;
else
    czyParzysta = false;
*/


//var czyParzysta = ;

alert((x % 2 === 0) ? "ta liczba jest parzysta" : "ta liczba jest nieparzysta");