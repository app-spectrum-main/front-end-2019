$(function(){
    var carouselList = $("#carousel ul");
	console.log('DOM loaded - you can have fun');
	function moveFirstSlide() {
        var firstItem = carouselList.find("li:first");
        var lastItem = carouselList.find("li:last");
        lastItem.after(firstItem);
        carouselList.css({marginLeft:0});
	};
	setInterval(changeSlides, 3000);
    function changeSlides() {
        carouselList.animate({'marginLeft':-400}, 1000, moveFirstSlide);
	};
    $("#next").click(function() {
        changeSlides();
    });
}); 