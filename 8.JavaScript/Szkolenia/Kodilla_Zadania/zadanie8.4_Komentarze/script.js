var variable = 'test';
console.log(variable);

// linear comment

// Below I have commented on a function call to disable it
// console.log('The commented will not be called');

/*
  block comment
  console.log('The commented will not be called');
*/