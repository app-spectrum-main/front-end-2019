var femaleNames = ['Asia', 'Kasia', 'Ola', 'Jola'];
var maleNames = ['Piotrek', 'Marek', 'Arek', 'Jarek'];
var allNames = femaleNames.concat(maleNames);

function addName(name) {
    if (allNames.indexOf(name) === -1)
        allNames.push(name);
}

function getNamesThatStartWith(letter){
    return allNames.filter(name => name[0] == letter)
}

function getNamesThatAreLongerThan(exclusiveMinLength){
    return allNames.filter(name => name.length > exclusiveMinLength)
}

function getSumOfLengths(exclusiveMinLength){
    var lengths = allNames.map(name => name.length);
    return lengths.reduce((a, b) => a + b, 0);
}

function getMapOfNamesAndLengths(){
    return allNames.map(name => ({"name":name, "length": name.length}));
}

function reverseNames(){
    allNames = allNames.reverse();
}

function cutBetween(startIndex, endIndex){
    allNames.splice(startIndex, endIndex-startIndex+1);
}

function sortNames(){
    allNames.sort();
}

function getUnique(someArray){
    return someArray.filter((item, pos, self) => self.indexOf(item) == pos);
}

console.log("Names", allNames);
var newName = 'Patryk';
addName(newName);
console.log("Names after adding Patryk", allNames);

console.log("Names wich start with M", getNamesThatStartWith("M"));
console.log("Names longer than 5", getNamesThatAreLongerThan(5));
console.log("Sum of all name lengths", getSumOfLengths());
console.log("Names with lengths", getMapOfNamesAndLengths());

reverseNames();
console.log("Names after reversion of order", allNames);
cutBetween(2, 4);
console.log("After cut ", allNames);
sortNames();
console.log("Sorted ", allNames);

var allNamesWithCopies = JSON.parse(JSON.stringify(allNames));
allNamesWithCopies.push(allNames[0]);
allNamesWithCopies.push(allNames[2]);
allNamesWithCopies.push(allNames[2]);
allNamesWithCopies.push(allNames[4]);
console.log("With duplicates", allNamesWithCopies);
console.log("After remove of duplicates", getUnique(allNamesWithCopies));
