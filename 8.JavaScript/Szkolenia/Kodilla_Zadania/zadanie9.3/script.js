var text = "Velociraptor is a genus of herbivorous ceratopsid dinosaur that first appeared during the " +
    "late Maastrichtian stage of the late Cretaceous period.";
//not true

console.log('That is not true, It is about Triceratops.');
var dinosaur = 'triceratops';

var dinosaurUpperCased = dinosaur.toUpperCase();
console.log(dinosaur.toUpperCase());

var textAfter = text.replace ('Velociraptor', dinosaur);
console.log(textAfter);

console.log(textAfter.length);

var textHalf = textAfter.substr(0, (textAfter.length)/2);
console.log(textHalf);

function getFirstWords(someText, nWords){
    var words = someText.split(" ");
    return words.slice(0, nWords).join(" ");
}

function reverse(someText){
    return someText.split("").reverse().join("");
}

function removeWhiteSpace(someText){
    return someText.split(" ").reverse().join("");
}

function naiveReplace(someText, source, target){
    return someText.replace(source, target);
}

function replaceAllTimes(someText, source, target){
    return someText.split(source).join(target);
}

function getLetterPostions(someText, letter){
    return someText.split("").map( (c, i) => (c == letter) ? i : -1).filter(pos => pos >= 0);
}

console.log("first 5 words", getFirstWords(text, 5));
console.log("reversed", reverse(text));
console.log("no whitespace", removeWhiteSpace(text));
console.log("naive replace: first -> 1st", naiveReplace(text, "first", "1st"));
console.log("replace all: whitespace -> underscore ", replaceAllTimes(text, " ", "_"));
console.log("replace all: Maastrichtian -> TakieCoś ", replaceAllTimes(text, "Maastrichtian", "TakieCoś"));
console.log("all positions of letter 'a'", getLetterPostions(text, "a"));
