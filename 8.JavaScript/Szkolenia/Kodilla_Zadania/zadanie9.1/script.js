function getTriangleArea(a, h) {
    if (a < 0 || h < 0) {
        return "Invalid data";
    }
    return a*h/2;
}
// function getTriangleArea(a, h) {
//     if (a <= 0 && h <= 0) {
//         return "Invalid data";
//     }
//     else if (a > 0 && h > 0) {
//         return(a*h/2);
//     }
// }
console.log(getTriangleArea(10, 6));
var triangle1Area = getTriangleArea(6, 15);
console.log(triangle1Area);
var triangle2Area = getTriangleArea(10, 15);
console.log(triangle2Area);
var triangle3Area = getTriangleArea(7, 5);
console.log(triangle3Area);
var triangle4AreaWrongData = getTriangleArea(-7, 5);
console.log(triangle4AreaWrongData);
