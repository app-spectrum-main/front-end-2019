## Kilka przykładów wykorzystania:
•	slider z obrazkami, które przełączają się co jakiś odstęp czasu, <br>
•	hamburger menu, wysuwane menu, <br>
•	elementy wykorzystujące kliknięcie, niepowodujące przeładowania strony, <br>
•	walidacja formularzy, <br>
•	dodanie produktów do koszyka, <br>
•	polubienie/łapka w górę, <br>
•	ukrycie elementu informującego o plikach cookie po kliknięciu przycisku, <br>
•	odliczanie czasu, często spotykane w sklepach internetowych, <br>
•	modal, czyli wyskakujące na stronie okienka, <br>
•	doczytywanie postów (nieskończony scroll), <br>
•	gry.

## Struktura katalogow
/workspace
/js/scripts.js
/css/style.css
/images
index.html

## JS - Hint
JS-hint

## Debug
alt + cmd + i - Konsola

## Czytelnośc kodu
https://github.com/airbnb/javascript

## Wstęp
var name; - zadeklarowanie zmiennej
prompt('Enter your name'); - wywołanie funkcji i przekazanie parametru
var name = prompt('Wpisz swoje imię'); = operacja przypisania wartości do zmiennej name

= - instrukcja przypisania

console.log(variable) - sprawdź wartość zmiennej

## Typy zmiennych
- Integer (Liczba) int lub float
- String (Tekst)
```js
var userName = 'Jan Kowalski';
```
- Boolean (true lub false) 
```js
var isActive = true;
```
- Tablice
```js
var hobby = ['HTML', 'CSS', 'JavaScript'];
```
Index tablicy
```js
var foo = test[2];
```

- Obiekty
```js
var object = {}; - pusty obiekt

{
atrybut: wartośc;
}

var object = {
    attribute: 'attribute value',
    otherAttribute: 123 + 456,
    attributeWithVariableValue: variable
};
```

## Odczytanie wartości obiektów
```js
var person = {
    name: 'Jon',
    surname: 'Snow',
    siblings: 5,
    knownThings: ['Nothing','Night Watch']
};

var userName = person.name;
var userSurname = person['surname'];

```

- undefined - niezdefiniowana wartość zmiennej
```js
var person;
console.log(person); // undefined
```
- null <br>
Null służy do wskazywania, że zmienna nie odnosi się do prawidłowych danych. Null jest specjalną wartością oznaczającą "wartość nieustaloną".

- NaN  <br>
Not a Number, czyli 'to nie jest liczba'. Występuje np. przy mnożeniu liczba razy string.


## Inkrementacja Dekrementacja
```js
var points = 1;
points += 1;

points = points - 1;
points -= 1;
```
## Instrukcje logiczne / if switch
- Operator równości wartości ==  <br>
Sprawdza, czy wartości po obu stronach są równe lub takie same.

- Operator równości wartości i typu ===  <br>
Sprawdza, czy wartości po obu stronach są równe lub takie same. Oraz czy typ wartości jest ten sam.

- Operator różności !=    <br>
Sprawdza, czy wartości po obu stronach są różne. Odwrotność operatora równości.

- Operator większy niż >    <br>
Sprawdza, czy wartość po lewej stronie jest większa niż wartość po prawej.

- Operator mniejszy niż <    <br>
Sprawdza, czy wartość po lewej stronie jest mniejsza niż wartość po prawej.

## && - Łącznik "i"
Pozwala na łączenie dwóch warunków ze sobą.
Aby wynik był prawdą, oba warunki składowe muszą być prawdą.

## || - Łącznik "lub"
Pozwala na łączenie dwóch warunków ze sobą.
Aby wynik był prawdą, przynajmniej jeden z warunków składowych musi być prawdą.

## Instrukcja Switch
```js
var question = prompt('Do you like strawberries?');

switch (question.toLowerCase()) {
  case 'yes':
    alert('Great, me too!');
    break;
  case 'no':
    alert('Really?');
    break;
  default:
    alert("You did not give a clear answer, so you're probably still wondering.");
}
```
## Short if
```js
(condition) ? code_if_condition_is_true : code_if_condition_is_false
```

# Webpack Frontend Starterkit
A lightweight foundation for your next webpack based frontend project.
### Installation
```
npm install
```
### Start Dev Server
```
npm start
```
### Build Prod Version
```
npm run build
```
### Features:
* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)
When you run `npm run build` we use the [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the `index.html`.

# Webpack Starter Kit
git clone https://github.com/wbkd/webpack-starter.git

## Babel
      // {
      //   test: /\.(js)$/,
      //   include: Path.resolve(__dirname, '../src'),
      //   loader: 'babel-loader'
      // },
      
# JavaScript Start
     - case sensitive
     - whitespace
     - variables
     - variable errors
     - String / Numeric 
     - Operators
     - Comments
     - 'Welcome to Blackjack we\'are'
     - 5 + '5' - nie wie że to liczba  wynik = 55
     - konkatenacja - łączenie stringów
     - Addition + / Substraction - / Multiplication * / Division / / Remainder % / Parenthesis ()
     - Types  typeof
     - Numbers / 4.1 + 4.3 = 8.39    /   10 / 0 = infinity -infinity   0/0 = NaN
     - Flags isMultiplayer = false;
     - undefined 
     - null   typeof null = object
     - Arrays   values.length  values.push(44); values.pop(); values.shift(); values.splice();
     - Conditionals Using if();
     - flasy: false , 0, "", null, undefined, NaN
     - truthy: true, 0.5, string
     - if ( score > 1000 ) { score + 100; }
     - if else
     - switch / case
     - for (let i = 0; i < 3; i++) {}
     - functions 
     - function showMessage() {
        console.log('In a function')
     }
     - function myFunction (message, favoriteNumber) {
        message = message + World;
        console.log(message, favoriteNumber);
     }
     myFunction ('Wiadomosc', 12);
     - function triplePlus(value) {
        let newValue = value + value + value;
        return newalue;
     }
     console.log(triplePlus(3));  // 9
     
     function myFunction(favouriteNumber) {
        let newNumber = favouriteNumber + 100;
        return newNumber;
     }
     
     let result = myFunction(42);
     console.log(result);
     - funcionScope 
     
     let message = "First Message!";
     
     function myMessage (message) {
        let newMessage = message + 'Patryk';
        return message;
     }
     
     myFunction();
     console.log(message);
     
     - Objects
     - let person = {
        name: "John",
        age: 32,
        partTime: false
     };
     console.log(person.name);
     
     - Array of Objects
     let cards = [
        {
           suit: "Hearts",
           value: "Queen"
        },
        {
            suit: "Clubs",
            value: "King"
        }
     ];
     
     console.log(cards[0].suit);
     - Built In Objects
     - Math (Random  Numbers)
     - Date (Date)
     - String (Manipulate strings)
     - Number (Manipulate numbers)
     
     let result = Math.random() * 52;
     result = Math.trunc(result);
     
     console.log(result);   
     
     
     let result = new Date().toDateString();
     console.log(result);
     
     let result = 0 / 0
     console.log( NUmber.isNan(result));    
     
     
     - DOM Document Object Model
     - Dodawanie tekstu do dom
     - let textArea = document.getElementById('text-area');
       textArea.innerText = "Siema Patryk";
     - Usuwanie / Dodawanie Elementów
     - let paragraph = document.getElementById('paragraph');
     - 
     
## Const
- musi być zainicjalizowana

## Var / Let (Hoisting)
    console.log(carId);   // error
    let carId = 42;

    console.log(carId);   // undefined
    let carId = 30;
    
## Rest Parameters
    function sendCars(...allCarIds) {                  // Tworzymy tablicę allCarIds
        allCarIds.forEach(id => console.log(id));      // używamy funkcji forEach wypisać elementy
    }
    
    sendCars(100, 200, 555);
    
    function sendCars(day, ..allCarIds) {
         allCarIds.forEach(id => console.log(id));
    }
    
    sendCars('Monday', 100, 200, 300);                 // Wypisze mi dni Monday i wartości   (Monday stanie się częścią tablicy
    
## Destructuring Array

    let carIds = [1, 2, 3];
    let [car1, car2, car3] = carIds;
    console.log(car1, car2, car3);
    
    // 1 2 5
    
    let carIds = [1, 2, 3];
    let car1, remainingCars;
    [car1, ...remainingCars] = carIds;
    // 1 [2, 5]
    
    let carIds = [1, 2, 3];
    let remainingCars;
    [, ... remainingCars] = carIds;                     // Pomiń pierwszą pozycję tabeli
    
    console.log(remainingCars);
    // [2, 5]
    
## Destructuring Objects

    let car = {id: 5, style: 'convertible' };
    let { id, style } = car;
    
    console.log(id, style);
    // 5000 convertible
    
    let car = {id: 5, style: 'convertible' };
    let id, style;
    ({ id, style } = car);
    
    console.log(id, style);
    // 5000
    
## Spread Syntax
    
    function startCars(car1, car2, car3) {
        console.log(car1, car2, car3);
    }
    
    let carIds = [100, 300, 500];
    starCars(...carIds);                                // Bierzemy Tablicę carIds i przypisujemy do niej elemnty car1 , car2 , car3
    // 100 300 500

## typeof()

    typeof(1);      // number
    typeof(true)    // boolean
    typeof('Helo')  // string
    typeof( function() {} ); // function
    typeof( {} )    // object
    typeof( null )  // object
    typeof( undefined )  // undefined
    typeof(NaN);    // number
    
    
## Common Type Conversions

    foo.toString();             // Convert to String
    Number.parseInt('55'):      // 55 as a number
    Number.parseFloat('55,99'); // 55.99 as a number    // Musi zaczynać się od liczby
    
 
## Controlling loops

    let i=0;
    for (; i<12; i++) {                                 // Nie inicjaluzujemy żadnych zmiennych nie jest to wymagane
        if (i === 8) {
            break;
        }
    }
    
    console.log(i); // 8                                // Zatrzymam na i = 8 i wyjdę z pętli do console.log
    
    
    for (let i=0; i<4; i++) {
        if (i === 2) {
            continue;
        }
        console.log(i);
    }
    
    // 0 1 2 
    
    
# Operators

## Equality Operators

    if (var1 == var2) {}                               // JavaScript try to change Type
    if (var1 === var2) {}       
    
    let id = 123;
    console.log(id !== "123");
    // true
    

## Unary Operators

    ++var1                    // Zmienna zostanie zwiększona zanim zostanie uzyta              
    var1++                    // Zmienna zostanie użyta a następnie zwiększona o 1
    
    --var1
    var1--
    
    +var1
    
    let year = "1987";
    console.log(typeof(+1987))  // Powinien byc string ale + konwertuje na Number
    
    -var1

## Logical Operators

    Boolean
    if (var1 > 5 && var2 < 100) {           // Jeżeli oba warunki prawdziwe to wykonaj blok
        console.log(true);
    } else {
        console.log(false);
    }   
    if (var1 > 5 || var2 < 100) {}    // Jeden z warunków musi zostać spełniony
    
    
    if (var1 > 5 || var2 < 100 && var3 === 5) {}    Symbol && ma wyższy priorytet od || 
    
    var1 && var2
    var1 || var 2 
    
    !var1
    
    let car = null;
    if ( !car ) {
        car = {};
    }
    

## Relational Operators
    // > < = <= >=
    

## Conditional Operators
    
    var result = (foo > 5) ? true: false;
    // Zamiast if else
    console.log( 5 > 44 ? 'yes' : 'no');
    
    
## Assignment Operators
    
    var1 += 10;
    var1 -= 10;
    var1 *= 10;
    var1 /= 10;
    var1%= 10;

## Operator Precedence

    
# Functions and Scope

## Function Scope
    
    function startCar(carId) {
        let message = 'Starting...';
    }
    
    startCar(123);
    console.log(message); // error
    
    function startCar(carId) {
        let message = 'Starting...';
        let startFn = function turnKey() {
            let message = 'Override';
        };
        startFn();
        console.log(message);  // 'Starting..'
    }
    startCar(123);
    
## Block Scope

    if (5 === 5 ) {
        let message = 'Equal';
    }
    
    console.log(message); // Error
    
    if (5 === 5 ) {
        let message = 'Equal';
        console.log(message); // Equal
    }
    
    
## IIFE's Pattern - Immediately Invoked Function Expression

    let app = (function() {
        let carId = 123;
        console.log('In Function')
        return {} ;
    })();
    
    
## Closures    |   Wrócić do tego !
    
    let app = (function() {
            let carId = 123;
            letgetId = function() {
                return carId;
            };
            return {
                get: getId
            };
    })();
    console.log(app.getId() );
    
## This keyword

    let o = {
        carId: 123,
        getId: function() {
            return this.carId;
        }
    };
    console.log(o.getId());      // Zwróci 123
    
    
     let o = {
            carId: 123,
            getId: function() {
                console.log(this);      // Zwróci { carId: 123, getId: f }
                return this.carId;
            }
     };
    
## Call and Apply  // Funkcja Wywołania Funkcja Zastosuj


    // Funkcja call
    
    let o = {
            carId: 123,
            getId: function() {
            return this.carId;
            }
    };
         
         
    let newCar = { carId: 456 };
    console.log(o.getId.call(newCar) );   // 456
         
    // Funkcja apply
    
    let o = {
        carId: 123,
        getId: function(prefix) {
            return prefix + this.carId;
        }
    };
    
    let newCar = { carId: 324 };
    console.log(o.getId.apply(newCar, ['ID:'  ]) );
    
    // ID: 324
    
    
## Bind      |  Wrócić do tego !
    
     let o = {
        carId: 123,
        getId: function() {
            return this.carId;
        }
     };
     
     let newCar = { carId: 456 };
     let newFn = o.getId.bind(newCar);
     console.log(newFn() );
        
## Arrow Functions                          // Nie ma problemu z this w arrow function
    
    let getId = () => 123;
    console.log(getId());  // 123
    
    let getId = (number, number2) => {
        return number + number2;
    }
    
    console.log(getId);
    
## Default Parameters / Interpolation
    
    let trackCar = function(carId, city='NY') {
      console.log(`Tracking ${carId} in ${city].`);  
    }; 
    
# Object / Arrays

## Constructor Function
    function Car (id) {
        this.carId = id;
    }
    
    let car = new Car(123);
    console.log(car.carId); // 123
    
## Prototypes
    
    function Car (id) {
            this.carId = id;
    }
    
    Car.prototype.start = function () {
        console.log('start: ' + this.carId);
    };
    
    let car - new Car (123);
    car.start();  // 123
    
    
## Objects using prototypes
    
    String.prototype.hello = function () {
        return this.toString() + 'Hello';
    };    
    
    console.log('foo'.hello()); // foo Hello
    
# JSON - JavaScript Object Notation
    
    let car = {
        id: 123,
        style: 'convertible'
    };
    
    console.log( JSON.stringify(car) );
    // { "id": 123, "style": "convertible"  }
    
    let carIds = [
        { carId: 123 },
        { carId: 456 },
        { carId: 789 } 
    ];
    
    console.log( JSON.stringyfy(carIds) );
    // [{ }]
    
## Parsowanie JSON

    let jsonIn = 
    `
    [
        { "carId": 123 },
        { "carId": 456 },
        { "carId": 789 } 
    ]
    `;
    
    let carIds = JSON.parse(jsonIn);
    console.log( carIds);
    
    console.log( JSON.stringyfy(carIds));
    
## Array Iteration  Wrócić do tego !
    carIds.forEach(car => console.log(car));
    
    
# Class
    class Car {
    
    }
    
    let car = new Car();
    console.log(car);
    
## Constructor and properties - new instance of a object created
    
    class Car {
        constructor(id) {
            this.id = id;
        }
    }
    
    let car = new Car(123);
    console.log(car.id);
    
    
    
        