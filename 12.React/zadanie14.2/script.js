////////////////Wstęp////////////////

// var element = React.createElement('div', {}, 'Hello world!');
// ReactDOM.render(element, document.getElementById('app'));
//
// var element2 = React.createElement('p', {}, 'Siema Kinga!');
// ReactDOM.render(element2, document.getElementById('app'));

// var element3 = React.createElement('div', {},
//     React.createElement('h1', {}, 'Lista filmów'),
//     React.createElement('ul', {},
//         React.createElement('li', {},
//             React.createElement('h2', {}, 'Harry Potter'),
//             React.createElement('p', {}, 'Film o czarodzieju')
//         ),
//         React.createElement('li', {},
//             React.createElement('h2', {}, 'Król Lew'),
//             React.createElement('p', {}, 'Film opowiadający historię króla sawanny')
//         )
//     )
// );
// ReactDOM.render(element3, document.getElementById('app'));

////////////////Zadanie////////////////

var movies = [
    {
        id: 1,
        title: 'Harry Potter',
        desc: 'Film o czarodzieju',
        img: 'harry.jpg'
    },{
        id: 2,
        title: 'Król Lew',
        desc: 'Film o krolu sawanny',
        img: 'krol.jpg'
    },
    {
        id: 3,
        title: 'Matrix',
        desc: 'Film SF o agentach',
        img: 'matrix.jpg'
    },
    {
        id: 4,
        title: 'Muminki',
        desc: 'Bajka w której gra włóczykij',
        img: 'muminki.jpg'
    }
];

var moviesElements = movies.map(function(movie){
   return React.createElement('li', {key: movie.id},
       React.createElement('h2', {}, movie.title),
       React.createElement('p', {}, movie.desc),
       React.createElement('img', { src: movie.img },)
   );
});

var element = React.createElement('div', {},
    React.createElement('h1', {}, 'Lista filmów'),
    React.createElement('ul', {}, moviesElements)
);

ReactDOM.render(element, document.getElementById('app'));
