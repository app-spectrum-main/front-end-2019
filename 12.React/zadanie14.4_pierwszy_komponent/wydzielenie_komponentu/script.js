var movies = [
    {
        id: 1,
        title: 'Harry Potter',
        desc: 'Film o czarodzieju',
        img: 'harry.jpg'
    },{
        id: 2,
        title: 'Król Lew',
        desc: 'Film o krolu sawanny',
        img: 'krol.jpg'
    },
    {
        id: 3,
        title: 'Matrix',
        desc: 'Film SF o agentach',
        img: 'matrix.jpg'
    },
    {
        id: 4,
        title: 'Muminki',
        desc: 'Bajka w której gra włóczykij',
        img: 'muminki.jpg'
    }
];

var Header = React.createClass({
    render: function() {
        return React.createElement('h1', {}, 'Lista filmów');
    }
});

var MovieDescription = React.createClass({
    propTypes: {
        desc: React.PropTypes.string.isRequired,
    },
    render: function() {
        return React.createElement('p', {}, this.props.desc);
    }
});

var MovieTitle = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired,
    },
    render: function() {
        return React.createElement('h2', {}, this.props.title);
    }
});

var Movie = React.createClass({
    propTypes: {
        movie: React.PropTypes.object.isRequired,
    },
    render: function() {
        return React.createElement('li', {key: this.props.movie.id},
            React.createElement(MovieTitle, {title: this.props.movie.title} ),
            React.createElement(MovieDescription, {desc: this.props.movie.desc}),
            React.createElement('img', { src: this.props.movie.img })
        );
    }
});

var MovieList = React.createClass({
    propTypes: {
        items: React.PropTypes.array.isRequired,
    },
    render: function(){
        var movies = this.props.items.map(function(movie){
            return React.createElement(Movie, {movie: movie, key: movie.id});
        });
        return React.createElement('ul', {}, movies);
    }
});

var App = React.createClass({
    render: function() {
        return (
            React.createElement('div', {},
                React.createElement(Header),
                React.createElement(MovieList, {items: movies}) )
        );
    }
});

var app = React.createElement(App);
ReactDOM.render(app, document.getElementById('app'));





// var moviesElements = movies.map(function(movie){
//     return React.createElement('li', {key: movie.id},
//         React.createElement('h2', {}, movie.title),
//         React.createElement('p', {}, movie.desc),
//         React.createElement('img', { src: movie.img },)
//     );
// });
//
// var element = React.createElement('div', {},
//     React.createElement('h1', {}, 'Lista filmów'),
//     React.createElement('ul', {}, moviesElements)
// );
//
// ReactDOM.render(element, document.getElementById('app'));
//
// var Header = React.createClass({
//     render: function() {
//         return React.createElement('h1', {}, 'Lista filmów');
//     }
// });
