### Selektory
https://www.w3schools.com/css/default.asp <br>
selector { property: value; property: value; }

### ZAGNIEZDŻANIE SELETKORÓW
```css
header a { /* Styles for the links inside header */ }
header .main-navigation a { /* Styles for the links inside .main-navigation inside header */ }
.example > div { /* Properties of divs that are direct inside .example */ }

```

### Grupowanie
```css
h1, h2, h3 { text-align: center; font-weight: bold; }

```

### Elementy rodzeńswa
```css
.example + p { /* adjacent sibling selector */ }
.example ~ p { /* general sibling selector */ }

```
### Pseudoklasy
```css
a:link {color: grey;}
a:hover { color: red; }
a:visited { color: green; }
a:active { color: #0000FF; }
```

#### PAMIĘTAJ: a:hover musi występować po a:link i a:visited w kolejności definiowania stylów w CSS. Podobnie a:active piszemy po a:hover!

### NT-y potomek
```css
p:first-child { /* First paragraph of parent div */ } 
li:last-child { /* Last item in the ul or ol list */ } 
p:nth-child(3) { /* Paragraph, being the n-th child of the parent - in this example it's third */ }
```

###CSS 3 Gradients
https://github.com/mrmrs/gradients <br>
http://www.css3factory.com/linear-gradients

### Prefixy
.grad {     background: red; /* for browsers that do not support gradients */     
background: -webkit-linear-gradient(red, yellow); /* Safari 5.1 - 6.0 */    
background: -o-linear-gradient(red, yellow); /* Opera 11.1 - 12.0 */     
background: -moz-linear-gradient(red, yellow); /* Firefox 3.6 do 15 */     
background: linear-gradient(red, yellow); /* standard syntax */ }

### Transition
```html
div {     width: 100px;     height: 100px;     background: red;     transition: width 2s; } 
div:hover {     width: 300px; }
```

## Jednostki miar
Jednostki Miar <br>

Najczęstsze jednostki miar: <br>
- px - najpopularniejszy jednostka, określająca wielkość w pikselach, np. dla monitora komputera,
- % - procentowa wartość jest zależna od kontekstu, np. szerokości nadrzędnego kontenera (rodzica),
- em - jest mnożnikiem aktualnego rozmiaru elementu nadrzędnego,
- rem - bazuje na podstawowym rozmiarze czcionki strony (16px domyślnie lub takim jaki jest przypisane dla tagu HTML)
- vw - bazuje na szerokości okna (Viewport Width) 1vw = 1/100 szerokości okna,
- vh - bazuje na wysokości okna (Viewport Height) 1vh = 1/100 wysokości okna.


## Fonty

Nazwy czcionek składające się z kilku słów podajemy w regule CSS w cudzysłowach 
(font-family: "Times New Roman";), a te składające się z jednego słowa bez cudzysłowów (font-family: Arial;).
<br>
<link href='adres czcionki na serwerze' rel='stylesheet' type='text/css'> @import url(link-do-czcionki);

### Podstawowe rodzaje czcionek to: <br>
- szeryfowe (serif) są czcionkami z dodatkowymi elementami ozdobnymi (np. kreseczki na dole i górze dużej litery 'i'), często używane w przypadku stron o charakterze humanistycznym (np. Times),
- bezszeryfowe (sans serif) są czcionkami bez dodatkowych elementów ozdobnych, najczęściej używane (np. Arial),
- o stałej szerokości (monospace) są czcionkami, w których każdy znak zajmuje taką samą przestrzeń, używana są do prezentacji kodu (np. Courier),
- pisanki (handwritting), czcionki imitujące pismo odręczne, spotykany np. na stronach o charakterze humorystycznym (np. Comic Sans).

### Łamanie lini
Zdarzają się jednak sytuacje kiedy chcemy aby dany fragment tekstu nie był dzielony na dwa lub więcej wierszy. Aby uzyskać taki efekt używamy właściwości white-space (rozumiane jako "spacje") z wartością nowrap (ang. nie zawijać).
Przestrzeń między literami  letter-spacing oraz word-spacing  Transformacje tekstu

### Wartości dla text-transform to:
- uppercase duże litery,
- lowercase małe litery,
- capitalize każda pierwsza litera wyrazu będzie duża, pozostałe będą takie jakie zostały wprowadzone.
Klasie hero zmień wysokość na równą całej wysokości okna. Nasz baner uzyskał 100% wysokości okna podglądu :) To modny zabieg na stronach głównych.

## Dynamiczne wyliczanie wartości
Funkcja calc() pozwala wykonywać podstawowe działania arytmetyczne. Wystarczy, że w nawiasach zapiszemy działanie, na przykład width: calc(100px - 75px); zwróci wartość 25px.
Zwróć uwagę na spacje w konstrukcji funkcji calc(). Pomiędzy liczbami i znakiem działania matematycznego, muszą być zawsze spacje!

### Dane do obliczeń:
Wysokość stopki wraz z marginesem: 82px.
Wysokość banera wraz z marginesem: 120px.
Suma wysokości powyższych elementów: 202px.
