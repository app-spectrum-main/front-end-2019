# TypeScript
- https://www.typescriptlang.org/
- tsc --version
- tsc --help
- tsc --init

### CLI
- tsc app.ts

### Pliki .d.ts
- pliki definicji (zawierają informację o typach) bez implementacji
- typings
- https://github.com/typings/typings

### tsconfig.json
- compiler options
- include or exclude files in compilation
- support configuration inheritance

### Literal String
    
    `
    function logPlayer(playerName) {
        console.log(`Zawodnik nazywa się: ${playerName}`);
    }
    
    `

# Typy
- boolean   true/false
- number   liczby zmiennoprzecinkowe / heksadecymalne / oktalne / binarne
- string      string      const tpl = `${x}, ${y}!`;  
- array       const arr1:Array<number> = [1, 2, 3];    /    const arr2:number[] = [1, 2, 3]; 
- tumple    const tuple:[number, string] = [1, 'd'];
- enum       

    enum Suit {  
        Spades,
        Hearts,
        Diamonds,
        Clubs
    };

    const cards:Suit = Suit.Spades; // 0 

- any

    const x:Array<any> = [];  
    x.push(1);  
    x.push('a');  
    x.push(new Date);  


- void

// Funkcje które nic nie zwracają

    function showAlert(text:string):void {  
        window.alert(text);
    }
    
### Aditional types
- void
- null
- undefined
- never
- any

## Klasy i Interfejsy <br> (Konstruktory, Dziedziczenie prototypowe)

    class X {  
        private pole:typ;
        constructor(wartosc:typ) {
            this.pole = wartosc;
        }
    }

    class Animal {  
        name:string;
    
        constructor(givenName:string) {
            this.name = givenName;
        }
    
        sayHello():string {  
                return `Hello, my name is ${this.name}!`;
        }
    }
    
    const dog = new Animal(‚Patryk’);  
    dog.sayHello() // 'Hello, my name is Patryk!’;  
 

## Private / Public

    class Animal {  
        private name:string;
    
        constructor(givenName:string) {
            this.name = givenName;
        }
    
        public sayHello():string {  
                return `Hello, my name is ${this.name}!`;
        }
    }
    
    
## Interface
- typ reprezentujący instancje
- funkcję konstruktora

        interface Message {  
            text:string;
            sender:string;
            receiver:string;
        }
        const message:Message = {  
            text: 'Hello',
            sender: 'Michal',
            receiver: 'Anna'
        };
        
        