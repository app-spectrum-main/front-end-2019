// Arrow function
var double = function (value) {
    return value * 2;
};
console.log(double(10));
// Function with default Params
var greet = function (name) {
    if (name === void 0) { name = "Patryk"; }
    console.log("Hello" + name);
};
greet();
greet("Ania");
// Spread Operator
var numbers = [1, -2, 33, 5];
console.log(Math.min.apply(Math, numbers));
// Spread Operator
var newArray = [66, 2];
newArray.push.apply(newArray, numbers);
console.log(newArray);
// Destructuring Arrays
var testResults = [3.89, 2.99, 1.38];
var result1 = testResults[0], result2 = testResults[1], result3 = testResults[2];
console.log(result1, result2, result3);
// Destructuring Objects
var scientist = { firstName: "Will", experience: 12 };
var firstName = scientist.firstName, experience = scientist.experience;
console.log(firstName, experience);
