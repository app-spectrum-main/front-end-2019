type BankAccount = {money: number, deposit: (value: number) => void };

let bankAccount: {money: number, deposit: (value: number) => void } = {
    money: 2000,
    deposit(value: number): void {
        this.money += value;
    }
};