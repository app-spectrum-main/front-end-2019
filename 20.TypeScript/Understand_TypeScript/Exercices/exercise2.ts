// Arrow function
const double = (value: number) => {
    return value * 2;
};

console.log(double(10));


// Function with default Params
const greet = (name = "Patryk") => {
    console.log("Hello" + name);
};

greet();
greet("Ania");

// Spread Operator
const numbers = [1, -2, 33, 5];
console.log(Math.min(...numbers));

// Spread Operator
const newArray = [66, 2];
newArray.push(...numbers);
console.log(newArray);


// Destructuring Arrays
const testResults = [3.89, 2.99, 1.38];
const [result1, result2, result3] = testResults;
console.log(result1, result2, result3);

// Destructuring Objects

const scientist = {firstName: "Will", experience: 12};
const {firstName, experience} = scientist;
console.log(firstName, experience);