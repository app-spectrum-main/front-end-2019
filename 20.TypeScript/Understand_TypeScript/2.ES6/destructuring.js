// Destructuring
console.log("DESTRUCTURING");
var myHobbies = ["Cooking", "Sports"];
var hobby1 = myHobbies[0], hobby2 = myHobbies[1];
console.log(hobby1, hobby2);
// const userData = {userName: "Max", age: 27};
// const {userName: myName, age: myAge} = userData;
// console.log(myName, myAge);
