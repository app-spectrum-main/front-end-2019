// Arrow Functions

console.log("ARROW FUNCTIONS");
const addNumbers = function (number1: number, number2: number): number {
    return number1 + number2;
};
console.log(addNumbers(10, 3));

const multiplyNumbers = (number1: number, number2: number) => number1 * number2;
console.log(multiplyNumbers(10, 3));

// const greet = () => {
//     console.log("Hello!");
// };
// greet();

const greetFriend = friend => console.log(friend);
greetFriend("Manu");