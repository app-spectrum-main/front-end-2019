// Block scope
function reset() {
    // console.log(variable);
    let variable = null;
    console.log(variable);
}

reset();
console.log(variable);