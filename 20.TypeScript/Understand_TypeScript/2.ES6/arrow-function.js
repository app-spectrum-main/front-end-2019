// Arrow Functions
console.log("ARROW FUNCTIONS");
var addNumbers = function (number1, number2) {
    return number1 + number2;
};
console.log(addNumbers(10, 3));
var multiplyNumbers = function (number1, number2) { return number1 * number2; };
console.log(multiplyNumbers(10, 3));
// const greet = () => {
//     console.log("Hello!");
// };
// greet();
var greetFriend = function (friend) { return console.log(friend); };
greetFriend("Manu");
