// Classes
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// class Person {                     // Classic
//     name: string;
//     private type: string;
//     protected age: number;
//
//     constructor(name: string) {
//         this.name = name;
//     }
//
// }
var Person = /** @class */ (function () {
    function Person(name, username) {
        this.username = username;
        this.age = 27; // Initialize property   = 27;
        this.name = name;
    }
    Person.prototype.printAge = function () {
        console.log(this.age);
    };
    Person.prototype.setType = function (type) {
        this.type = type;
        console.log(this.type);
    };
    return Person;
}());
var person = new Person('Patryk', 'Sanecki');
console.log(person.name, person.username);
person.printAge();
person.setType('Stonoga');
// Inheritance and Constructor
var Patryk = /** @class */ (function (_super) {
    __extends(Patryk, _super);
    function Patryk(username) {
        var _this = _super.call(this, "Patryk", username) || this;
        _this.name = "Janek";
        _this.age = 31;
        return _this;
    }
    return Patryk;
}(Person));
var patryk = new Patryk("Patryk"); // We get
console.log(patryk);
// Getters & Setters   -  Control Access to properites
var Plant = /** @class */ (function () {
    function Plant() {
        this._species = "Default";
    }
    Object.defineProperty(Plant.prototype, "species", {
        get: function () {
            return this._species;
        },
        set: function (value) {
            if (value.length > 3) {
                this._species = value;
            }
            else {
                this._species = "Default";
            }
        },
        enumerable: true,
        configurable: true
    });
    return Plant;
}());
var plant = new Plant();
console.log(plant.species);
plant.species = "AB";
console.log(plant.species);
plant.species = "Green Plant";
console.log(plant.species);
// Static Properites & Methods
var Helpers = /** @class */ (function () {
    function Helpers() {
    }
    return Helpers;
}());
