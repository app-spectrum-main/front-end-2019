// Classes

// class Person {                     // Classic
//     name: string;
//     private type: string;
//     protected age: number;
//
//     constructor(name: string) {
//         this.name = name;
//     }
//
// }

class Person {                      // Better
    name: string;
    private type: string;           // Only inside Class
    protected age: number = 27;          // Initialize property   = 27;

    constructor(name: string, public username: string) {
        this.name = name;
    }

    printAge() {                    // Creating methods
        console.log(this.age);
    }

    setType(type: string) {
        this.type = type;
        console.log(this.type);
    }
}

const person = new Person('Patryk', 'Sanecki');
console.log(person.name, person.username);

person.printAge();
person.setType('Stonoga');


// Inheritance and Constructor

class Patryk extends Person {       // Take person Class
    name = "Janek";

    constructor(username: string) {
        super("Patryk", username);
        this.age = 31;
    }

}

const patryk = new Patryk("Patryk");   // We get
console.log(patryk);


// Getters & Setters   -  Control Access to properites

class Plant {
    private _species: string = "Default";

    get species() {
        return this._species;
    }

    set species(value: string) {      //  Set species value
        if (value.length > 3) {
            this._species = value;
        } else {
            this._species = "Default";
        }
    }



}

let plant = new Plant();
console.log(plant.species);
plant.species = "AB";
console.log(plant.species);

plant.species = "Green Plant";
console.log(plant.species);


// Static Properites & Methods
class Helpers {

}