## Dobre praktyki komponowania layoutu
Co do zasady, front-endowiec nie jest projektantem, lecz pewna fundamentalna wiedza z zakresu projektowania graficznego jest przydatna, jeśli nie niezbędna. Dzięki niej możemy lepiej porozumieć się z grafikiem, a także samemu rozpoznać dobre (lub niekoniecznie dobre) rozwiązania i wykorzystać tę wiedzę do sprawniejszego budowania strony. Poniżej znajduje się lista dobrych praktyk związanych z tworzeniem layoutu strony od podstaw.
### KISS - Keep it simple, stupid!
Jedną z najważniejszych zasad, które powinniśmy sobie przyswoić, jest dążenie do prostoty. Skomplikowany interfejs jest trudny w użytkowaniu i zniechęca użytkowników (i klientów!) do korzystania z naszej strony. Ponadto prostszy HTML i CSS powoduje, że strona wczytuje się w przeglądarce szybciej, nie skazując użytkowników na czekanie.
### Korzystaj ze znanych wzorców.
Omówiliśmy już zalety korzystania ze sprawdzonych wzorców. To, że znajdują się one na wielu stronach, wynika z tego, że przetrwały one próbę czasu. Możemy założyć, że jeżeli coś sprawdza się na stronie z milionami użytkowników, sprawdzi się też na mniejszą skalę.
### Ustal wizualną hierarchię.
Istotną sprawą jest to, aby uwaga użytkownika była skupiona na tym, co jest najważniejsze. W kontekście layoutu ma to znaczenie zwłaszcza przez ustalenie kolejności i rozmiarów poszczególnych treści, ich rozmiarów, kolorów, odstępów pomiędzy elementami.
### Nie każ użytkownikom myśleć.
Jest to założenie podobne do pierwszego, jednak nie do końca. Twój layout powinien być zrozumiały dla użytkownika, czyli funkcja poszczególnych jego elementów powinna być jasna od momentu ich ujrzenia. Ponadto użytkownik powinien być w stanie znaleźć bez problemu potrzebne mu funkcje i treści.
### Zadbaj o czytelność tekstu.
Twój tekst powinien mieć dostatecznie duży rozmiar zapewniający czytelność na każdym ekranie. Ponadto interlinie oraz odstępy między literami powinny być dostatecznie duże, by tekst oraz litery nie najeżdżały na siebie.
### Spraw by Twój tekst oddychał! 
Nie żałuj odstępów i marginesów, by zapewnić każdemu elementowi odpowiednią przestrzeń. Pamiętaj też o odpowiednim kontraście pomiędzy tłem a tekstem. Projektując stronę, trzeba pamiętać o starszych użytkownikach ze słabszym wzrokiem. Słabszy kontrast ogranicza czytelność tekstu przy wyświetlaniu na mniejszych ekranach. Także wtedy gdy na wyświetlacz świeci słońce lub gdy tekst wyświetlany jest na starszym monitorze.
Konsorcjum W3C ustanowiło pewne minimalne standardy w tej kwestii: minimalny kontrast pomiędzy tekstem a tłem powinien wynosić 4,5 : 1.
Możemy sprawdzić kontrast np. za pomocą tego narzędzia.

## Siatka
![GRID](../image/grid.png)

### Box sizing
```css
* { box-sizing: border-box; } /*doliczane będą ich dopełnienia (padding) oraz obramowanie (border). */

.row:before, .row:after { content:""; 	display: table; clear: both; }

[class*='col-'] { float: left; Min-height: 1px; padding: 12px;  }


```

```html
<div class="col-8"> 	
    <div class="row"> 	    
        <div class="col-4">A column with a width of 1/3 of the parent column</div> 	   
        <div class="col-4">A column with a width of 1/3 of the parent column</div> 	    
        <div class="col-2">A column with a width of 1/6 of the parent column</div> 	    
        <div class="col-2">A column with a width of 1/6 of the parent column</div> 
    </div> 
</div>
```


```css
.col-1 {width: 8.33%;}
 .col-2 {width: 16.66%;} 
.col-3 {width: 25%;}
 .col-4 {width: 33.33%;} 
.col-5 {width: 41.66%;} 
.col-6 {width: 50%;}
 .col-7 {width: 58.33%;}
 .col-8 {width: 66.66%;} 
.col-9 {width: 75%;}
 .col-10 {width: 83.33%;} 
.col-11 {width: 91.66%;} 
.col-12 {width: 100%;}
```

```css
.col-offset-0 {margin-left: 0;}
 .col-offset-1 {margin-left: 8.33%;}
 .col-offset-2 {margin-left: 16.66%;}
 .col-offset-3 {margin-left: 25%;} 
.col-offset-4 {margin-left: 33.33%;} 
.col-offset-5 {margin-left: 41.66%;} 
.col-offset-6 {margin-left: 50%;}
 .col-offset-7 {margin-left: 58.33%;}
 .col-offset-8 {margin-left: 66.66%;}
 .col-offset-9 {margin-left: 75%;}
 .col-offset-10 {margin-left: 83.33%;}
 .col-offset-11 {margin-left: 91.66%;}
```

###Ogólny układ
```html
<!-- HTML5 tag with our class, for example features, about, jumbotron --> 
<section class="about"> 	<div class="container"> 	    
<!-- clearfix - row must be the parent of the columns! --> 		
<div class="row">                    
 <div class="col-6"></div>                 
     <div class="col-6"></div> 	       
 </div> 	
</div>
 </section>
```

## Pixel perfect
Kolejną przeszkodą do pokonania na drodze od projektu graficznego do gotowej strony jest pojawiająca się wątpliwość dotycząca tego, do jakiego stopnia szczegółowości odwzorowywać projekt graficzny na stronie internetowej, czyli pytanie o tzw. pixel perfect. W wolnym tłumaczeniu pojęcie pixel perfect oznacza, że projekt graficzny został odwzorowany z dokładnością do pojedynczego piksela. Perfekcja odwzorowania świadczy o wysokiej jakości wykonanej pracy. Jak łatwo się domyślić, odwzorowanie wszystkiego z dokładnością co do piksela bywa trudne lub praktycznie niemożliwe i również wymaga poświęcenia temu odpowiedniego czasu w komunikacji ze zleceniodawcą.
Spróbujmy wyobrazić sobie, że ktoś otwiera naszą stronę na czarno-białym czytniku Kindle, a ktoś inny na 60-calowym telewizorze z UltraHD. Nie ma technicznej możliwości, żeby to wyglądało identycznie. Rozważając zagadnienie perfekcyjnego odwzorowania projektu graficznego, trzeba wziąć pod uwagę skalę projektu i jego koszty oraz przede wszystkim oczekiwania klienta.

