# Preprocesory
- https://sass-lang.com/

## Instalacja
http://sass-lang.com/install <br>
sudo gem install sass
sass -v

### Kompilacja
sass --watch input.scss:output.css

## Less
http://lesscss.org

## Sass
https://sass-lang.com

### W przypadku składni Sass 
reguły budowane są z formatowaniem, jakiego uczyliśmy się dotychczas, z tą jednak różnicą, że omijamy średniki oraz nawiasy klamrowe. Istnieją też drobne różnice przy korzystaniu z poszczególnych funkcji.
### SCSS reprezentuje nieco odmienne podejście
jego składnia wygląda dokładnie tak, jak składnia CSS i nie obsługuje on pomijania średników oraz nawiasów klamrowych.

## Zmienne
```scss
$fonts: "Montserrat", sans-serif;
$kodilla-green: #53B863;
span {
    font: 100% $fonts;
    color: $kodilla-green;
}

h1 {
    font: 24px/36px $fonts;
    background-color: $kodilla-green;
}
```

## Zagnieżdżanie
```scss
nav {
    ul {
        margin: 0;
        padding: 0;
        list-style: none;
    }
    li {
        display: inline-block;
        width: 100px;
        height: 50px;
        text-align: center;
    }
    a {
        display: block;
        padding: 6px 12px;
        text-decoration: none;
        background-color: #2c3e50;
        color: white;
    }
}
```
## Grid
http://oddbird.net/susy

@import partials
@import susy

## Mixins
```scss
@mixin border-radius($radius) {
  -webkit-border-radius: $radius;
  -moz-border-radius: $radius;
  -ms-border-radius: $radius;
  border-radius: $radius;
}
.box {
  @include border-radius(10px);
}
.circle {
  @include border-radius(50%);
}

/* Po kompilacji*/
.box {
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  -ms-border-radius: 10px;
  border-radius: 10px;
}
.circle {
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  -ms-border-radius: 50%;
  border-radius: 50%;
}

```

## Operatory
```scss
.message {
  border: 3px solid #ccc;
  padding: 10px;
  color: #333;
  width: 200px;
  text-align: center;
  height: 50px;
  margin: 0 auto;
}

.success {
  @extend .message;
  border-color: green;
}

.error {
  @extend .message;
  border-color: red;
}

.warning {
  @extend .message;
  border-color: yellow;
}
```