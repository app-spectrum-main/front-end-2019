# Klasa
```js
function Car(model, manufacturer) { //class properties are given in the parameter
   //we assign values from function parameters to properties
   this.model = model;
   this.manufacturer = manufacturer;
}
Car.prototype.logModel = function(){
    console.log("This car's model is called " + this.model + ".");
};
```
# Obiekt
```js
var fordMustang = new Car("Mustang", "Ford");

fordMustang.logModel();
//console: "This car's model is called Mustang."
```

# Prymitywy 
Są reprezentowane przez samą wartość, nie mają żadnych metod ani właściwości.
<br>
Przykładowo: <br> 
 "Hello World" // String
 123 // Number
 false // Boolean
 
# Literały


# Konstruktory
Object() i String(), Number(), Date(), Boolean() 
```js
var name = new Object("Jan Nowak");

/*Obiekt*/
var person = {} // literal
var person = new Object() // constructor
/*Tablica*/
var people = [] // literal
var people = new Array() // constructor
/*Deklaracja obiektu*/
var person = {
	name: "Jan Nowak",
	age: 30,
	isMarried: false
};
/*Metoda*/
var person = {
	name: "Jan Nowak",
	age: 30,
	isMarried: false,
	sayHello: function() {
		console.log("Hi, my name is " + this.name);
    }
};
person.sayHello(); // "Hi, my name is Jan Nowak"

/*Funkcja konstruujaca - Klasa*/
function Person(name, age) {
	this.name = name;
	this.age = age;
	this.sayHello = function() {
		console.log("Hi, my name is " + this.name);
    };
};

var person1 = new Person("Jan Nowak", 35);
var person2 = new Person("Adam", 32);
var person3 = new Person("Ewa", 18);

/*Metoda wewnątrz klasy*/
function Smartphone(brand, model) {
	this.brand = brand;
	this.model = model;
	this.logModel = function() {
		console.log(this.model);
	}
}

iPhone6.logModel(); //call method for iPhone6 object

/*Prototyp*/
function Smartphone(brand, model) {
	this.brand = brand;
	this.model = model;
}
Smartphone.prototype.logModel = function() {
	console.log(this.model);
};

iPhone6.logModel();

function Car(color, brand) { //construction functions are capitalized!
	this.color = color; //creating a new object, we can specify its initial value using the color property
	this.brand = brand || 'ford'; //if no brand is given at this point, the default value is 'ford'
}
```

# this (kontekst)
Wskazuje na kontekst w ktorym operuje nasza funkcja

## SELF
```js
/*Gubienie kontekstu*/
var person = {
	name: 'Jan',
	sayHello: function() {
    var self = this;
	setTimeout(function(){
		console.log('Hello '  + self.name + '!');
        }, 1000)
    }
};
person.sayHello() // Hello Jan!
```
