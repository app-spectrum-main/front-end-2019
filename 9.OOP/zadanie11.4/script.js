function Phone(brand, price, color, weight) {
    this.brand = brand;
    this.price = price;
    this.color = color;
    this.weight = weight;
}

Phone.prototype.printInfo = function () {
    console.log("The phone brand is " + this.brand + ", color is" + this.color + "." + "Weight " + this.weight + " and the price is " + this.price + ".");
};

Phone.prototype.printWeight = function () {
    console.log(this.brand + " Weight is " + this.weight)
};

var iPhone6S = new Phone("Apple", 2250, "silver", "450g");
var samsungGalaxy6 = new Phone("Samsung", 3000, "gold", "200g");
var onePlusOne = new Phone("OnePlus", 1900, "black", "230g");

iPhone6S.printInfo();
samsungGalaxy6.printInfo();
onePlusOne.printWeight();
