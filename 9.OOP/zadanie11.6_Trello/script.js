$(document).ready(function() {
    var idList = []; // globalna lista identyfikatorow
    var archColumn = null;// globalny link do kolumny archiwizacji
    console.log('DOM loaded-ready for work');
    function randomString() {
        var chars = '0123456789abcdefghiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXTZ';
        var str = '';
        while (idList.indexOf(str) != -1){ // iteruje dopuki str jest w liscie idList
            str = ''; // indexOf zwraca polozenie elementu w liscie lub -1 jesli elementu nie ma w liscie
            for (var i = 0; i < 10; i++) {
                str += chars[Math.floor(Math.random() * chars.length)];
            }
        }
        idList.push(str);// dodaje dobry indetyfikator do listy
        return str;
    }
    function Column(data) {// podaje slownik z argumentami
        var self = this;
        this.id = randomString();
        this.name = data["name"];// wczesniej bylo function Column(name) a potem this.name=name
        this.type = data["type"];
        this.$element = createColumn();
        if (data["type"] == "arch")
            self.hide();// kolumna arch jest schowana na poczatku
        function createColumn() {
            var $column = $('<div>').addClass('column');
            var $columnTitle = $('<h2>').addClass('column-title').text(self.name);
            var $columnCardList = $('<ul>').addClass('column-card-list');
            // w kolumnie archiwizacji nie bedzie dalo sie dodac karty klikajac
            var $columnAddCard = null; // dlatego zmienna na poczatku jest null
            // nie bedzie dalo sie usunac kolumny archiwizacji klikajac
            var $columnDelete = null;
            if (data["type"] != "arch") { // a jesli typ kolumny NIE jest arch to pojawi sie guzik dodawania karty
                // uwaga na nawias
                $columnAddCard = $('<button>').addClass('add-card').text('Add a card');
                $columnAddCard.click(function(event) {
                    var promptOut = prompt("Enter the name of the card"); // prompt to metoda wywolujaca okienko
                    // do funkci prompot trzeba podac argument: wiadomosc do uzytkownika (patrz specyfikacja js)
                    // jesli rzytkownik kliknie OK prompt zwraca to co wpiszesz w linijke
                    // jesli kliknie cancel zwraca null.
                    // no to sprawdzamy :)
                    if (promptOut === null) // potrojne = to znaczy ze sprawdzamy wartosc i typ!
                        return;//jak jest null to wychodzimy z callbacka
                    self.addCard(new Card(promptOut));
                });
                $columnDelete = $('<button>').addClass('btn-delete').text('x');
                $columnDelete.click(function() { // tylko kolumni ktore NIE sa typu arch bedzie mozna usuwac
                    self.removeColumn();
                });
            };
            $column.append($columnTitle)
                .append($columnDelete) // dla kolumny tupu arch to jest null, ale nawet jak to dodam do HTML to nic sie nie stanie
                .append($columnAddCard) // jak powyzej
                .append($columnCardList);
            return $column;
        }
    }
    Column.prototype = {
        addCard: function(card) {
            this.$element.children('ul').append(card.$element);
            if (this.type != "arch")// jesli nowa kolumna karty nie jest typu arch
                card.column = this;// columna bedzie wiedziec w ktorej jest karcie
            // w ten sposub karta bedzie wiedziec gdzie byla przed archiwizacja
        },
        removeColumn: function() {
            this.$element.remove();
        },
        show: function() { // Dodaje do prototypu kolumny show/hide
            this.$element.show();
        },
        hide: function() {
            this.$element.hide();
        }
    };
    function Card(description) {// podaje wskaznik do kolumny archiwizacji
        var self = this;
        this.id = randomString();
        this.description = description;
        this.$element = createCard();
        this.column = null;// w jakiej kolumnie jest karta, na poczatku w zadnej
        function createCard() {
            var $card = $('<li>').addClass('card');
            var $cardDescription = $('<p>').addClass('card-description').text(self.description);
            var $cardDelete = $('<button>').addClass('btn-delete').text('x');
            var $cardArch = $('<button>').addClass('btn-arch').text('arch');
            var $cardRevertArch = $('<button>').addClass('btn-revert-arch').text('revert-arch');
            $cardRevertArch.hide(); // domyslnie karta jest w swojej kolumnie a nie w archiwum,
            //wiec nie ma jeszcze czego cofac
            $cardDelete.click(function() {
                self.removeCard();
            });
            $cardArch.click(function() {// handler klikniecia w guzik archiwizacji
                // dodaje karte w ktorej urzytkownik kliknal guzik archiwizacji
                archColumn.addCard(self);// self czyli synonim this
                //this sie nadpisal w tym callbacku bo javascript jest zjebany
                // ona sie nie kopiuje tylko sie przenosi, jak bysmy wywolali self.removeCard() to by znikla
                archColumn.show();// kolumna archColumn jest domyslnie schowana wiec trzeba ja pokazac
                // bo cos juz w niej jest
                $cardRevertArch.show();// jak jest w archiwum to guzik cofnij ma byc widoczny
                $cardArch.hide();// jak karta JUZ jest w archiwum, nie ma sensu pokazywac tego guzika
            });
            $cardRevertArch.click(function() {// handler klikniecia w guzik powrotu z archiwizacji
                if (self.column === null)
                    return;
                self.column.addCard(self);// przerzuca karte do
                $cardRevertArch.hide();// jak wrocila z archiwum to guzik cofnij ma sie schowac
                $cardArch.show();// a guzik zarchiwizuj ma byc widoczny (moze ktos sie rozmysli)
            });
            var $comment = $('<input>').addClass('comment');
            $comment.prop('disabled', true);// domyslnie zablokowane edytowanie
            var $commentEditButton = $('<button>').addClass('btn-edit-comment').text('edit comment');
            $commentEditButton.click(function() {// callback-handler klikniecia w guzik edycji komentarza
                var newState = !$comment.prop('disabled');// wykrzynkik znaczy negacja, zamienia true na false i vice versa
                $comment.prop('disabled', newState);// czyli jesli edytowanie bylo zablokowane to sie odblokuje
                // a jesli bylo odblokowane to sie zablokuje
                if ($comment.prop('disabled'))// tekst guzika edycji zmienia sie w zaleznosci od sytuacji
                    $commentEditButton.text("edit comment");// jesli edycja jest zablokowana
                else
                    $commentEditButton.text("submit");// jesli edycja jest mozliwa
            });
            // tworzymy guzik do edycji komentarza
            $card.append($cardDelete)
                .append($cardDescription)
                .append($cardArch).append($cardRevertArch)// dodalem guzik do archiwizacji do HTMLa i de-archiwizacji
                .append($comment)// dodajemy komentarz;
                .append($commentEditButton);// guzik do edycji komentarza
            return $card;
        }
    }
    Card.prototype = {
        removeCard: function() {
            this.$element.remove();
        }
    }
    var board = {
        name: 'Kanban Board',
        addColumn: function(column) {
            this.$element.append(column.$element);
            initSortable();
        },
        $element: $('#board .column-container')
    };
    function initSortable() {
        $('.column-card-list').sortable({
            connectWith: '.column-card-list',
            placeholder: 'card-placeholder'
        }).disableSelection();
    }
    $('.create-column')
        .click(function() {
            var name = prompt('Enter a column name');
            var column = new Column({"name":name, "type":"standard"});
            board.addColumn(column);
        });
    // zmienilem psudo-constructor zeby przyjmowal obiekt zmiast tekstu
    //dlatego zmieniam tez instancjonowanie
    //bylo tak: var todoColumn = new Column('To do');
    // a teraz jest tak:
    archColumn = new Column({"name":'Arch', "type": "arch"});// dodajemy kolumne archiwum-X
    var todoColumn = new Column({"name":'To do', "type": "standard"}); // typ standard
    var doingColumn = new Column({"name":'Doing', "type": "standard"});
    var doneColumn = new Column({"name":'Done', "type": "standard"});


    // kolumna typu arch
    board.addColumn(todoColumn);
    board.addColumn(doingColumn);
    board.addColumn(doneColumn);
    board.addColumn(archColumn);

    var card1 = new Card('New task');
    var card2 = new Card('Create kanban boards');

    todoColumn.addCard(card1);
    doingColumn.addCard(card2);
});


//Wersja pierwsza
// $(document).ready(function() {z
//     console.log('DOM loaded-ready for work');
//     function randomString() {
//         var chars = '0123456789abcdefghiklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXTZ';
//         var str = '';
//         for (var i = 0; i < 10; i++) {
//             str += chars[Math.floor(Math.random() * chars.length)];
//         }
//         return str;
//     }
//     function Column(name) {
//         var self = this;
//         this.id = randomString();
//         this.name = name;
//         this.$element = createColumn();
//         function createColumn() {
//             var $column = $('<div>').addClass('column');
//             var $columnTitle = $('<h2>').addClass('column-title').text(self.name);
//             var $columnCardList = $('<ul>').addClass('column-card-list');
//             var $columnDelete = $('<button>').addClass('btn-delete').text('x');
//             var $columnAddCard = $('<button>').addClass('add-card').text('Add a card');
//             $columnDelete.click(function() {
//                 self.removeColumn();
//             });
//             $columnAddCard.click(function(event) {
//                 self.addCard(new Card(prompt("Enter the name of the card")));
//             });
//             $column.append($columnTitle)
//                 .append($columnDelete)
//                 .append($columnAddCard)
//                 .append($columnCardList);
//             return $column;
//         }
//     }
//     Column.prototype = {
//         addCard: function(card) {
//             this.$element.children('ul').append(card.$element);
//         },
//         removeColumn: function() {
//             this.$element.remove();
//         }
//     };
//     function Card(description) {
//         var self = this;
//         this.id = randomString();
//         this.description = description;
//         this.$element = createCard();
//         function createCard() {
//             var $card = $('<li>').addClass('card');
//             var $cardDescription = $('<p>').addClass('card-description').text(self.description);
//             var $cardDelete = $('<button>').addClass('btn-delete').text('x');
//             $cardDelete.click(function() {
//                 self.removeCard();
//             });
//             $card.append($cardDelete)
//                 .append($cardDescription);
//             return $card;
//         }
//     }
//     Card.prototype = {
//         removeCard: function() {
//             this.$element.remove();
//         }
//     }
//     var board = {
//         name: 'Kanban Board',
//         addColumn: function(column) {
//             this.$element.append(column.$element);
//             initSortable();
//         },
//         $element: $('#board .column-container')
//     };
//     function initSortable() {
//         $('.column-card-list').sortable({
//             connectWith: '.column-card-list',
//             placeholder: 'card-placeholder'
//         }).disableSelection();
//     }
//     $('.create-column')
//         .click(function() {
//             var name = prompt('Enter a column name');
//             var column = new Column(name);
//             board.addColumn(column);
//         });
//     var todoColumn = new Column('To do');
//     var doingColumn = new Column('Doing');
//     var doneColumn = new Column('Done');
//
//     board.addColumn(todoColumn);
//     board.addColumn(doingColumn);
//     board.addColumn(doneColumn);
//
//     var card1 = new Card('New task');
//     var card2 = new Card('Create kanban boards');
//
//     todoColumn.addCard(card1);
//     doingColumn.addCard(card2);
// });
