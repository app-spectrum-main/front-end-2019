# Project FRONT END 2019

# Courses
- http://how2html.pl (Beginner)

## Pluralsight
- Building Stronger Front-end Practices  (Intermediate)

# UX / UI  (Amber Israelsen)
- UX Role and Impact on Organizations
- User Experience: The Big Picture
- User Experience Tips and Tricks for Developers
- UX Fundamentals by Krispian Emert
- User Experience: The Big Picture
- Getting Started in UX Design

### CSS Path
- HTML , CSS, JavaScript Big Picture (Beginner)
- Introduction to CSS (Basic)
- CSS Positioning (Beginner)
- Responsive Typografy (Beginner)             ***In progress***
- Better CSS: LESS and SASS (Intermediate)        ***Revisit***
- Hands on Responsive Web Design (Intermediate):
 - responsive images / sliders
 - media queries / css preprocessing
- Responsive Web Design by Ben Callahan
### SCSS
- Better CSS: LESS and SASS

### JavaScript Path
- HTML , CSS, JavaScript Big Picture (Beginner)
- JavaScript Gettig Started (Beginner)
- JavaScript Fundamentals  (Intermediate)     ***In progress***
- JavaScript Objects and Prototypes (Intermediate)   ***TODO***
- Practical Design Patterns    (Intermediate)        ***TODO***
- Rapid ES6 Training

## Node.js
- Introduction to Node.js

## Typescript
- TypeScript Fundamentals
- TypeScript In-Depth

## Angular Path
- Angular BigPicture       (Beginner)         
- Angular Getting Started  (Beginner)
- Angular CLI (2018)                  ***TODO***
- Angular 2 Forms                     ***TODO***
- Angular 6 Routing          (Intermediate)
- Angular 2 Reactive Forms   (Intermediate) ***In progress***  
- Angular 4 HTTP Communication  (Intermediate) ***In progress*** 
- Angular 4 Services            (Intermediate) ***In progress*** 
- Angular 4 Best Practises
- Angular 2 Building Responsive Framework

# Ionic
- Building Mobile Apps with Ionic, Angular and TypeScript

# Devtools
- NPM Playbook
- 

# Graphic
- Illustrator CC for Creative Professionals
- InDesign CC for Creative Professionals
- Graphic Design

- Photoshop CC Fundamentals by Mattew Pizzi
- Photoshop CC Practical Projects by Jesus Ramirez
- Photoshop CC Selections ***In progress***

## Strefa kursów

## Udemy
- Kurs HTML 5
- Kurs tworzenia stron www HTML / CSS / JavaScript od podstaw do eksprerta
- Kurs CSS 3 Zaawansowany
- Kurs CCC 3 Transformacje

# Spis treści

## [1.HTML 5](1.HTML5/HTML5.md) 
## [2.HTML 5](2.CSS3/CSS.md) 
## [3.GRID](3.GRID/GRID.md)
## [4.RWD](4.RWD/RWD.md)
## [5.Bootsrap](5.Bootstrap/Bootstrap.md)
## [6.SASS](6.SASS/SASS.md)
## [7.Narzędzia](7.Narzedzia_devloperskie/Narzedzia.md)
## [8.JavaScript](8.JavaScript/JavaScript.md)
## [10.jQuery](jQuery.md)
## [11.OOP](11.OOP/OOP.md)
## [12.Ajax/API](12.Ajax_API/Ajax_API.md)
## [13.Node.js](13.NodeJS/NodeJS.md)
## [14.React.js]()
## [15.EcmaScript6]()
## [16.Webpack]()
## [17.ExpressJS]()
## [18.Czat]()
## [19.Redux]()
## [20.React-Router]()
## [21.MongoDB](21.MongoDB/)
## [22.MERN](22.MERN/mern.md)
## [23.Kanban](23.Kanban)
